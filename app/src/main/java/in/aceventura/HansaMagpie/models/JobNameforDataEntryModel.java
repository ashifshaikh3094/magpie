package in.aceventura.HansaMagpie.models;

import java.io.Serializable;

public class JobNameforDataEntryModel implements Serializable {
    String address, district, entryDateTime, fieldCenter, fieldOffice, interviewerId, jobName, recordingTime, recordingType, remarks, respondentName, respondentPhoneNo, segment, state, usedStatus, id;

    public JobNameforDataEntryModel(String address, String district, String entryDateTime, String fieldCenter, String fieldOffice, String interviewerId, String jobName, String recordingTime, String recordingType, String remarks, String respondentName, String respondentPhoneNo, String segment, String state, String usedStatus, String id) {
        this.address = address;
        this.district = district;
        this.entryDateTime = entryDateTime;
        this.fieldCenter = fieldCenter;
        this.fieldOffice = fieldOffice;
        this.interviewerId = interviewerId;
        this.jobName = jobName;
        this.recordingTime = recordingTime;
        this.recordingType = recordingType;
        this.remarks = remarks;
        this.respondentName = respondentName;
        this.respondentPhoneNo = respondentPhoneNo;
        this.segment = segment;
        this.state = state;
        this.usedStatus = usedStatus;
        this.id = id;
    }

    public JobNameforDataEntryModel() {

    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getDistrict() {
        return district;
    }

    public void setDistrict(String district) {
        this.district = district;
    }

    public String getEntryDateTime() {
        return entryDateTime;
    }

    public void setEntryDateTime(String entryDateTime) {
        this.entryDateTime = entryDateTime;
    }

    public String getFieldCenter() {
        return fieldCenter;
    }

    public void setFieldCenter(String fieldCenter) {
        this.fieldCenter = fieldCenter;
    }

    public String getFieldOffice() {
        return fieldOffice;
    }

    public void setFieldOffice(String fieldOffice) {
        this.fieldOffice = fieldOffice;
    }

    public String getInterviewerId() {
        return interviewerId;
    }

    public void setInterviewerId(String interviewerId) {
        this.interviewerId = interviewerId;
    }

    public String getJobName() {
        return jobName;
    }

    public void setJobName(String jobName) {
        this.jobName = jobName;
    }

    public String getRecordingTime() {
        return recordingTime;
    }

    public void setRecordingTime(String recordingTime) {
        this.recordingTime = recordingTime;
    }

    public String getRecordingType() {
        return recordingType;
    }

    public void setRecordingType(String recordingType) {
        this.recordingType = recordingType;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public String getRespondentName() {
        return respondentName;
    }

    public void setRespondentName(String respondentName) {
        this.respondentName = respondentName;
    }

    public String getRespondentPhoneNo() {
        return respondentPhoneNo;
    }

    public void setRespondentPhoneNo(String respondentPhoneNo) {
        this.respondentPhoneNo = respondentPhoneNo;
    }

    public String getSegment() {
        return segment;
    }

    public void setSegment(String segment) {
        this.segment = segment;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getUsedStatus() {
        return usedStatus;
    }

    public void setUsedStatus(String usedStatus) {
        this.usedStatus = usedStatus;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
