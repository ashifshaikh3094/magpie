package in.aceventura.HansaMagpie.models;


public class DataModel {
    String record;
    String status;

    public DataModel(String record, String status) {
        this.record = record;
        this.status = status;
    }

    public DataModel() {
    }

    public String getRecord() {
        return record;
    }

    public void setRecord(String record) {
        this.record = record;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}