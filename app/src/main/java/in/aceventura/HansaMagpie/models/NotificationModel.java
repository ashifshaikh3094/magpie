package in.aceventura.HansaMagpie.models;

public class NotificationModel {
    String Type;
    String Text;


    public NotificationModel(String Type,String Text) {
        this.Type = Type;
        this.Text = Text;
    }

    public NotificationModel() {

    }


    public String getType() {
        return Type;
    }

    public void setType(String Type) {
        this.Type = Type;
    }

    public String getText() {
        return Text;
    }

    public void setText(String Text) {
        this.Text = Text;
    }
}

