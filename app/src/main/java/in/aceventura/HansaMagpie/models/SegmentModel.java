package in.aceventura.HansaMagpie.models;

public class SegmentModel {
    String SegName,SegTotal,SegRej;

    public String getSegName() {
        return SegName;
    }

    public void setSegName(String segName) {
        SegName = segName;
    }

    public String getSegTotal() {
        return SegTotal;
    }

    public void setSegTotal(String segTotal) {
        SegTotal = segTotal;
    }

    public String getSegRej() {
        return SegRej;
    }

    public void setSegRej(String segRej) {
        SegRej = segRej;
    }

    public SegmentModel(String segName, String segTotal, String segRej) {
        this.SegName = segName;
        this.SegTotal = segTotal;
        this.SegRej = segRej;
    }
}
