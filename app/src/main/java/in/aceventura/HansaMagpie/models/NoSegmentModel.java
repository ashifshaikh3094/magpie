package in.aceventura.HansaMagpie.models;

/* renamed from: in.aceventura.HansaMagpie.models.NoSegmentModel */
public class NoSegmentModel {
    String SegRej;
    String SegTotal;

    public String getSegTotal() {
        return this.SegTotal;
    }

    public void setSegTotal(String str) {
        this.SegTotal = str;
    }

    public String getSegRej() {
        return this.SegRej;
    }

    public void setSegRej(String str) {
        this.SegRej = str;
    }

    public NoSegmentModel(String str, String str2) {
        this.SegTotal = str;
        this.SegRej = str2;
    }
}
