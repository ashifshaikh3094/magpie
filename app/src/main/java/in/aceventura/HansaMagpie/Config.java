package in.aceventura.HansaMagpie;

public class Config {
    //Test Server url
    // public static final String PROJECT_URL = "http://203.188.224.233/Service1.svc/";

    //Live Server url
    public static final String PROJECT_URL = "http://203.115.122.103/SMSAPI/Service1.svc/";

    static final String LOCAL_ANDROID_VERSION = "4.3";

    static final String LOCAL_ANDROID_VERSION_DAILOG_TITLE = "Update Required";
    static final String LOCAL_ANDROID_VERSION_MESSAGE = "New update is available";

}
