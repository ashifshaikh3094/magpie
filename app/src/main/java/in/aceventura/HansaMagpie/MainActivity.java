package in.aceventura.HansaMagpie;

import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.telephony.SmsManager;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.material.snackbar.Snackbar;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import in.aceventura.HansaMagpie.NewMenus.ActivityNotifications;
import in.aceventura.HansaMagpie.util.SharedPrefManager;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {
    private EditText edtMobno, editTextConfirmOtp;
    private String edtMobOtpno, edtotp, Intid;
    private DBManager dbManager;
    private ProgressDialog progressDialog;
    private String DATABASE_NAME = "UserData.db";
    private RequestQueue requestQueue;
    private ArrayList<AddStudentBean> addStudentBeansArraylist;
    private AddStudentBean addStudentBean;
    LinearLayout linearLayout;
    public String userid, pwd, uName, pass, userMobile, userId;

    SharedClass sharedClass;

    public String notification = Config.PROJECT_URL + "Notification";
    RequestQueue mQueue;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        TextView txtIntvName = findViewById(R.id.txtIntName);
        requestQueue = Volley.newRequestQueue(getBaseContext());
        addStudentBean = new AddStudentBean();
        progressDialog = new ProgressDialog(this);
        addStudentBeansArraylist = new ArrayList<>();

        sharedClass = new SharedClass(MainActivity.this);
        Intid = sharedClass.loadSharedPreference_UserId();
        pwd = SharedPrefManager.getInstance(this).getPwd();


        dbManager = new DBManager(MainActivity.this, "UserData.db", null, 1);
        addStudentBeansArraylist = dbManager.getAllMobileNo();

        String intName = sharedClass.loadSharedPreference_IntName();
        txtIntvName.setText(intName);

        edtMobno = findViewById(R.id.edtUsername);
        edtMobno.setEnabled(false); //Interviewer will not be able to enter mobile directly here, it'll come from user


        edtMobno.setFocusable(false);
        edtMobno.setClickable(false);

        //Getting respondent's details
        Bundle fromUserRegistration = getIntent().getExtras();
        if (fromUserRegistration != null) {
            userMobile = fromUserRegistration.getString("respMobile");
            userId = fromUserRegistration.getString("userid");
            pwd = fromUserRegistration.getString("pwd");
        }

        edtMobno.setText(userMobile);  //Setting the mobile number directly from registration page

        Button btnnext = findViewById(R.id.btnnext);
        linearLayout = findViewById(R.id.linear);
        btnnext.setOnClickListener(this);
        mQueue = Volley.newRequestQueue(this);

        Intent intent = new Intent(null, Uri.parse("some data"), this, ActivityNotifications.class);
        intent.putExtra("from_notification", true);
        intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        PendingIntent contentIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);

    }


    // TODO: "IMPORTANT" Update the utils/Config - "LOCAL_ANDROID_VERSION" code
    // TODO: "IMPORTANT" Update the build.gradle(Module:app) - "versionName" and "versionCode"
    //  before uploading on the playstore.....

    private void otpVerifysync() {

        addStudentBeansArraylist = dbManager.getAllMobileNo();
        String count = String.valueOf(addStudentBeansArraylist.size());

        progressDialog.setMessage("Verify URN...");
        progressDialog.setCanceledOnTouchOutside(false);

        progressDialog.show();
        if (addStudentBeansArraylist.size() > 0) {
            Intid = addStudentBeansArraylist.get(0).getInt_Id();
            edtMobOtpno = addStudentBeansArraylist.get(0).getMobile_No();
            edtotp = addStudentBeansArraylist.get(0).getOTP();
            HashMap<String, String> params = new HashMap<>();
            params.put("Int_Id", Intid);
            params.put("Mobile_No", edtMobOtpno);
            params.put("OTP", edtotp);
            String url = Config.PROJECT_URL + "VerifySMS";

            JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(url, new JSONObject(params), response -> {

                progressDialog.dismiss();
                String IsVerified = "True";

                try {
                    if (response.getString("IsVerified").equalsIgnoreCase(IsVerified)) {

                        setpojo1();
                        dbManager.update(addStudentBean);
                        addStudentBeansArraylist.remove(0);
                        if (addStudentBeansArraylist.size() > 0) {
                            new Handler().postDelayed(() -> otpVerifysync(), 5000);
                            Toast.makeText(MainActivity.this, "URN verified successfully..", Toast.LENGTH_SHORT).show();

                        } else {
                            Toast.makeText(MainActivity.this, "Data Sync Finished..", Toast.LENGTH_SHORT).show();
                        }


                    } else {

                        Toast.makeText(MainActivity.this, "URN not verified..", Toast.LENGTH_SHORT).show();

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }


            }, error -> {

            });
            jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(
                    30000,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            requestQueue.add(jsonObjectRequest);
        } else {
            progressDialog.dismiss();
            Toast.makeText(this, "No Data to Sync..", Toast.LENGTH_SHORT).show();
        }
    }


    @Override
    public void onClick(View view) {
        edtMobOtpno = edtMobno.getText().toString();
        if (isNetworkConnected()) {
            if (validate()) {
                OtpGenerate();
            }
            //Write web services code for otp generation
        } else {
            if (validate()) {
                String phoneNo = "9220092200";
                String messag = "HANSA";
                send(phoneNo, messag, Intid, edtMobOtpno);
            }
        }

    }

    //directly send the SMS if offline
    //Built-in device default SMS application

    private void send(String phoneNo, String msg, String intid, String mobno) {

        String msgTosend = msg + " " + "," + intid + "," + mobno + "," + "Offline";
        Uri uri = Uri.parse("smsto:" + phoneNo);
        Intent intent = new Intent(Intent.ACTION_SENDTO, uri);
        intent.putExtra("sms_body", msgTosend);
        startActivity(intent);
        /* Initiates the SMS compose screen, because the activity contain ACTION_VIEW and sms uri */
    }

    // Check the below method's functioning
    //SmsManager API
    //Send direct sms not from default SMS Application

    public void sendSMS(String phoneNo, String msg, String intid, String mobno) {
        try {
            SmsManager smsManager = SmsManager.getDefault();
            smsManager.sendTextMessage(phoneNo, null, msg + " " + "," + intid + "," + mobno + "," + "Offline", null, null);

            Toast.makeText(getApplicationContext(), "Message Sent..", Toast.LENGTH_LONG).show();
            confirmOtp();
        } catch (Exception ex) {
            Toast.makeText(getApplicationContext(), ex.getMessage(), Toast.LENGTH_LONG).show();
            ex.printStackTrace();
        }
    }

    private boolean isNetworkConnected() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        return cm.getActiveNetworkInfo() != null;

    }

    private void confirmOtp() {
        //Creating a LayoutInflater object for the dialog box
        LayoutInflater li = LayoutInflater.from(this);
        //Creating a view to get the dialog box
        View confirmDialog = li.inflate(R.layout.custom_dialog_otp_lay, null);

        //Initizliaing confirm button fo dialog box and edittext of dialog box
        Button buttonConfirm = confirmDialog.findViewById(R.id.buttonConfirm);
        editTextConfirmOtp = confirmDialog.findViewById(R.id.editTextOtp);

        //Creating an alertdialog builder
        final AlertDialog.Builder alert = new AlertDialog.Builder(this);

        //Adding our dialog box to the view of alert dialog
        alert.setView(confirmDialog);

        //Creating an alert dialog
        final AlertDialog alertDialog = alert.create();

        //Displaying the alert dialog
        alertDialog.show();

        buttonConfirm.setOnClickListener(view -> {
            //Webservice for verify OTP
            if (isNetworkConnected()) {

                progressDialog.setMessage("Please Wait...");
                progressDialog.show();
                edtMobOtpno = edtMobno.getText().toString();
                edtotp = editTextConfirmOtp.getText().toString();

                HashMap<String, String> params = new HashMap<>();
                params.put("Int_Id", Intid);//Hardcoded
                params.put("Mobile_No", edtMobOtpno);
                params.put("OTP", edtotp);

                String url = Config.PROJECT_URL + "VerifySMS";
                JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(url, new JSONObject(params), response -> {

                    progressDialog.dismiss();
                    String IsVerified = "True";

                    try {
                        if (response.getString("IsVerified").equalsIgnoreCase(IsVerified)) {
                            Toast.makeText(MainActivity.this, "URN verified successfully..", Toast.LENGTH_SHORT).show();
                            alertDialog.dismiss();
                            //edtMobno.setText("");
                            Intent intent = new Intent(MainActivity.this, UserRegistrationActivity.class);
                            startActivity(intent);
                            finish();

                        } else {
                            Toast.makeText(MainActivity.this, "URN not verified successfully..", Toast.LENGTH_SHORT).show();

                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }


                }, error -> {

                });

                requestQueue.add(jsonObjectRequest);
            } else {
                Snackbar snackbar = Snackbar
                        .make(linearLayout
                                , "No internet connection!", Snackbar.LENGTH_LONG)
                        .setAction("Save", view1 -> {

                            setpojo();
                            long l = dbManager.insertStudent(addStudentBean);
                            if (l > 0) {
                                edtMobno.setText("");
                                Toast.makeText(MainActivity.this, "Saved Successfully", Toast.LENGTH_SHORT).show();
                            }

                        });

                snackbar.setDuration(5000);
                snackbar.show();

            }
        });
    }

    private void OtpGenerate() {
        progressDialog.setMessage("Please Wait...");
        progressDialog.setCanceledOnTouchOutside(false);

        progressDialog.setCancelable(false);
        progressDialog.show();

        edtMobOtpno = edtMobno.getText().toString();

        HashMap<String, String> params = new HashMap<String, String>();
        params.put("Int_Id", Intid);
        params.put("Mobile_No", edtMobOtpno);
        params.put("ComingFrom", "Online");

        String url = Config.PROJECT_URL + "CreateSMS";
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(url, new JSONObject(params), response -> {


            progressDialog.dismiss();

            String flag = "False";

            try {
                if (response.getString("sFlag").equalsIgnoreCase(flag)) {
                    String msg = response.getString("MSG");

                    //Toast.makeText(MainActivity.this, "Otp does not sent to Mobile Number", Toast.LENGTH_SHORT).show();
                    AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
                    {
                        builder.setCancelable(true);
                        builder.setTitle("Alert");
                        builder.setMessage("URN for " + edtMobOtpno + " " + "has failed " + "due to " + msg);
                        builder.setPositiveButton("OK", (dialog, which) -> dialog.dismiss());

                        builder.create().show();

                    }

                } else {
                    try {
                        progressDialog.dismiss();
                        String a = response.getString("Int_Id");
                        Toast.makeText(MainActivity.this, "URN Sent to Mobile Number", Toast.LENGTH_SHORT).show();

                        confirmOtp();


                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }, error -> Toast.makeText(MainActivity.this, "Timeout", Toast.LENGTH_SHORT).show());

        jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(
                500000000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(jsonObjectRequest);
    }

    private void setpojo() {
        addStudentBean = new AddStudentBean();
        addStudentBean.setMobile_No(edtMobno.getText().toString());
        addStudentBean.setOTP(editTextConfirmOtp.getText().toString());
        addStudentBean.setIsSync("NO");
        addStudentBean.setInt_Id(Intid);
    }

    private void setpojo1() {
        addStudentBean = new AddStudentBean();
        addStudentBean.setMobile_No(addStudentBeansArraylist.get(0).getMobile_No());
        addStudentBean.setOTP(addStudentBeansArraylist.get(0).getOTP());
        addStudentBean.setIsSync("YES");
        addStudentBean.setInt_Id(addStudentBeansArraylist.get(0).getInt_Id());
        addStudentBean.setId(addStudentBeansArraylist.get(0).getId());
    }

    public boolean validate() {
        boolean status = true;

        String ed_text = edtMobno.getText().toString().trim();

        if (ed_text.isEmpty() || ed_text.length() != 10 || ed_text.length() == 0 || ed_text.equals("") || ed_text == null) {
            edtMobno.setError("you must be enter valid number and could not be empty");
            edtMobno.setFocusable(true);
            status = false;
        }
        return status;
    }

    /*@Override
    public void onBackPressed() {
        finish();
    }*/

    @Override
    public void onBackPressed() {
        moveTaskToBack(true);
    }

}