package in.aceventura.HansaMagpie;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

public class ActivityPayment extends AppCompatActivity implements AdapterView.OnItemSelectedListener {

    public String cURL = Config.PROJECT_URL + "CenterName";
    public String pURL = Config.PROJECT_URL + "JobName";
    public String payURL = Config.PROJECT_URL + "PaymentStatus";

    Spinner Payment, Payment2;
    TextView PayStatus, PaidByAccounts, PaymentGenerated, PaymentProcessed;
    public static String CenterName;
    public static String JobName;
    public String Cname;
    List<String> Cnames = new ArrayList();
    public String job;
    List<String> Pnames = new ArrayList<>();
    RequestQueue mQueue;
    String projectItem;
    ArrayAdapter<String> pAdapter;
    ArrayAdapter<String> cAdapter;
    ArrayList aList;
    String uName;
    ProgressDialog progressDialog;


    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payment);
        uName = SharedClass.getInstance(this).loadSharedPreference_UserId();

        Payment = findViewById(R.id.PaySpinner);
        Payment2 = findViewById(R.id.PaySpinner2);

        PayStatus = findViewById(R.id.tvpaymentstatus);
        PaidByAccounts = findViewById(R.id.PaidByAccounts);
        PaymentGenerated = findViewById(R.id.PaymentGenerated);
        PaymentProcessed = findViewById(R.id.PaymentProcessed);

        progressDialog = new ProgressDialog(this);

        mQueue = Volley.newRequestQueue(this);


        final HashMap<String, String> params = new HashMap<>();
        params.put("uname", uName);
        params.put("type", "Payment");
        progressDialog.show();
        progressDialog.setMessage("Loading");
        progressDialog.setCanceledOnTouchOutside(false);


        JsonObjectRequest request = new JsonObjectRequest(pURL, new JSONObject(params), new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                // Toast.makeText(ActivityQCS.this, ""+response, Toast.LENGTH_SHORT).show();
                try {
                    JobName = response.getString("JobName");
                    // Toast.makeText(ActivityPayment.this, ""+JobName, Toast.LENGTH_SHORT).show();
                    aList = new ArrayList(Arrays.asList(JobName.split(",")));
                    for (int i = 0; i < aList.size(); i++) {
                        job = (String) aList.get(i);
                        if (!JobName.equals("null")) {
                            Pnames.add(job);
                            progressDialog.dismiss();
                        } else {
                            progressDialog.dismiss();
                            return;
                        }
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, error -> {

        });
        mQueue.add(request);

        Payment.setOnItemSelectedListener((ActivityPayment.this));
        Pnames.add(0, "Select JobName");
        pAdapter = new ArrayAdapter<>(this,
                android.R.layout.simple_spinner_dropdown_item, Pnames);
        pAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        Payment.setAdapter(pAdapter);

        Payment2.setOnItemSelectedListener((ActivityPayment.this));
        Cnames.add(0, "Select Center");
        cAdapter = new ArrayAdapter<>(this,
                android.R.layout.simple_spinner_dropdown_item, Cnames);
        cAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        Payment2.setAdapter(cAdapter);

    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int i, long id) {

        if (parent.getId() == R.id.PaySpinner) {

            if (parent.getItemAtPosition(i).equals("Select JobName")) {
                return;

            } else {
                cAdapter.clear();
                Cnames.add(0, "Select Center");
                projectItem = parent.getItemAtPosition(i).toString();
                cAdapter.notifyDataSetChanged();
                getCenter(projectItem);

            }

        } else if (parent.getId() == R.id.PaySpinner2) {
            if (parent.getItemAtPosition(i).equals("Select Center")) {
                return;
            } else {
                //operations
                Cname = parent.getItemAtPosition(i).toString();
                getStatus(Cname);
            }
        }

    }

    private void getCenter(String projectItem) {
        // Cnames.clear();
        final HashMap<String, String> params = new HashMap<>();
        params.put("uname", uName);
        params.put("JobName", projectItem);
        params.put("type", "Payment");
        progressDialog.show();
        progressDialog.setMessage("Loading");
        progressDialog.setCanceledOnTouchOutside(false);

        //Toast.makeText(this, ""+params, Toast.LENGTH_SHORT).show();
        JsonObjectRequest request2 = new JsonObjectRequest(cURL, new JSONObject(params),
                response -> {
                    //Toast.makeText(ActivityQCS.this, ""+response, Toast.LENGTH_SHORT).show();
                    try {

                        CenterName = response.getString("CenterName");
                        aList = new ArrayList(Arrays.asList(CenterName.split(",")));
                        for (int i = 0; i < aList.size(); i++) {
                            String job = (String) aList.get(i);
                            Cnames.add(job);
                            progressDialog.dismiss();
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }, error -> {

        });
        mQueue.add(request2);
    }

    private void getStatus(String Cname) {
        final HashMap<String, String> params = new HashMap<>();
        params.put("uname", uName);
        params.put("JobName", projectItem);
        params.put("centerName", Cname);
        progressDialog.show();
        progressDialog.setMessage("Loading");
        progressDialog.setCanceledOnTouchOutside(false);


        JsonObjectRequest request1 = new JsonObjectRequest(payURL, new JSONObject(params),
                response -> {
//                        Toast.makeText(ActivityPayment.this, ""+response, Toast.LENGTH_SHORT).show();
                    //Toast.makeText(ActivityPayment.this, ""+params, Toast.LENGTH_SHORT)
                    // .show();
                    try {
                        //PayStatus.setText(response.getString("PaymentStatus"));
                        PaidByAccounts.setText(response.getString("PaidByAccounts"));
                        PaymentGenerated.setText(response.getString("PaymentGenerated"));
                        PaymentProcessed.setText(response.getString("PaymentProcessed"));
                        progressDialog.dismiss();

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }, error -> error.printStackTrace());
        mQueue.add(request1);

    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }
}
