package in.aceventura.HansaMagpie;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.android.volley.RequestQueue;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import in.aceventura.HansaMagpie.util.SharedPrefManager;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener {
    SharedClass sharedClass;
    TextView tvVersion, tvpw, pp;
    String userid, passwprd, versionName;
    RequestQueue requestQueue, versionRequestQueue;
    ProgressDialog progressDialog;

    String ServerUploadPath = Config.PROJECT_URL + "DeviceInfo";

    String userId;
    String pwd;
    private EditText edtUserid, edtUsrpassword;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        sharedClass = new SharedClass(this);
        progressDialog = new ProgressDialog(this);
        requestQueue = Volley.newRequestQueue(getBaseContext());
        versionRequestQueue = Volley.newRequestQueue(getBaseContext());

        edtUserid = findViewById(R.id.edtUId);
        edtUsrpassword = findViewById(R.id.edtUPwd);
        Button btnlogin = findViewById(R.id.btnLogin);
        tvVersion = findViewById(R.id.tvVersion);
        pp = findViewById(R.id.pp);

        /*// TODO: 09-03-2020 Saving Today's Date in SharedPref
        Date c = Calendar.getInstance().getTime();
        SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy");
        String formattedDate = df.format(c);
        SharedClass.loginDate(formattedDate);*/ //saving in the sharedPref

        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            passwprd = bundle.getString("password");
            userid = bundle.getString("username");

            edtUsrpassword.setText(passwprd);
            edtUserid.setText(userid);
            Log.e("INT", "Values==" + userid);
            Log.e("INT", "Values==Edt" + edtUserid.getText().toString());


        }
        Button resetPassword = findViewById(R.id.reset_password);
        resetPassword.setOnClickListener(v -> {
            Intent intent = new Intent(LoginActivity.this, PhoneNumberActivity.class);
            startActivity(intent);

        });
        //Version code
        try {
            versionName = getPackageManager().getPackageInfo(getPackageName(), 0).versionName;
            tvVersion.setText("v " + versionName);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }

        btnlogin.setOnClickListener(this);
        if (SharedClass.getInstance(this).isLoggedIn()) {
            Log.e("INT", "v1luesLogin=" + userid);
            Log.e("INT", "Shared Login Values" + SharedClass.getInstance(this).loadSharedPreference_UserId());
            startActivity(new Intent(this, UserRegistrationActivity.class).putExtra("userid", userid));
            finish();

        }

    }

    //PrivacyPolicy
    public void onClickPP(View view) {
        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://203.188.224.233/PrivacyPolicy.html"));
        startActivity(intent);
    }


    @Override
    public void onClick(View view) {
        Login();

        SharedPreferences preferences = getSharedPreferences(PhoneNumberActivity.MyPREFERENCES, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putBoolean(PhoneNumberActivity.LOGGEDIN_SHARED_PREF, false);
        editor.putString("uname", "");
        editor.putString("pwd", "");
        editor.apply();

    }

    private void Login() {
        // TODO: 09-03-2020 Saving Today's Date in SharedPref when user logged in
        Date c = Calendar.getInstance().getTime();
        SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy");
        String formattedDate = df.format(c);
        SharedClass.loginDate(formattedDate);

        progressDialog.setMessage("Please Wait...");
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.show();
        userid = edtUserid.getText().toString();
        passwprd = edtUsrpassword.getText().toString();
        Log.e("INT", "ValuesIn Login Method=" + userid);
        //to save pwd
        SharedPrefManager.getInstance(getApplicationContext()).savePwd(passwprd);
        final String token = SharedPrefManager.getInstance(this).getDeviceToken(); //getting token from SPref

        Map<String, String> params1 = new HashMap<>();
        params1.put("UserId", userid);
        params1.put("TokenId", token);

        Map<String, String> versionParams = new HashMap<>();
        versionParams.put("uname", userid);
        versionParams.put("Version", versionName);
        //Login URL
        String url = Config.PROJECT_URL + "VerifyLogin";
        String UserVersionInfo = Config.PROJECT_URL + "UserVersionInfo";

        final HashMap<String, String> params = new HashMap<>();
        params.put("uname", userid);
        params.put("pwd", passwprd);
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(url, new JSONObject(params), response -> {

            progressDialog.dismiss();

            String isVerified = "True";
            try {
                Log.e("Login", "Acresponce>" + response);
                if (response.getString("IsVerified").equalsIgnoreCase(isVerified)) {

                    //sending version if only login is successful
                    JsonObjectRequest versionJsonObjectRequest = new JsonObjectRequest(UserVersionInfo, new JSONObject(versionParams), versionResponse -> {
                        try {

                            JSONObject jsonObject=new JSONObject(response.toString());
                           ;
                            Log.e("ProfileValue", "ACno" +  jsonObject.getString("AC_No"));

                            if (jsonObject != null) {
                                String msg = jsonObject.getString("MSG");


                                SharedClass.getInstance(getApplicationContext()).setuserProfile(
                                        jsonObject.getString("AC_No"),
                                        jsonObject.getString("Address"),
                                        jsonObject.getString("Bank"),
                                        jsonObject.getString("BankAcName"),
                                        jsonObject.getString("Branch"),
                                        jsonObject.getString("Contact_Number"),
                                        jsonObject.getString("IfscCode"),
                                        jsonObject.getString("IntName")
                                );
                                System.out.println(msg);
                            }
                        } catch (JSONException e1) {
                            Log.e("ProfileValue", "error" + e1.getMessage());

                            e1.printStackTrace();
                        }

                    }, error -> {

                    });

                    versionRequestQueue.add(versionJsonObjectRequest);

                    try {
                        SharedClass.getInstance(getApplicationContext())
                                .userLogin(
                                        response.getString("uname"),
                                        response.getString("pwd"),
                                        response.getString("IntName"),
                                        response.getString("fieldOffice")

                                );

                        // TODO: 06-03-2020 redirect it to Registration Page and then to MainActivity
                        Intent intent = new Intent(LoginActivity.this, UserRegistrationActivity.class);
                        intent.putExtra("userid", userid);
                        intent.putExtra("pwd", passwprd);
                        startActivity(intent);
                        finish();

                        //====================================================================================

                        UploadToServerFunction();

                        //=================================================================

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    Toast.makeText(LoginActivity.this, "Login Successful", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(LoginActivity.this, "Invalid Credentials", Toast.LENGTH_SHORT).show();

                }
            } catch (JSONException e1) {
                e1.printStackTrace();
            }

        }, error -> {

        });

        requestQueue.add(jsonObjectRequest);
    }

    public void UploadToServerFunction() {

        final String token = SharedPrefManager.getInstance(this).getDeviceToken();

        Map<String, String> params = new HashMap<>();
        params.put("UserId", userid);
        params.put("DeviceId", userid);
        params.put("TokenId", token);

        JsonObjectRequest req = new JsonObjectRequest(ServerUploadPath, new JSONObject(params),
                response -> {
                    try {
                        VolleyLog.v("Response:%n %s", response.toString(4));
                    } catch (JSONException e) {
                        e.printStackTrace();

                        Toast.makeText(LoginActivity.this, "" + response, Toast.LENGTH_SHORT).show();

                    }
                }, error -> VolleyLog.e("Error: ", error.getMessage()));

        RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
        requestQueue.add(req);
    }

}
