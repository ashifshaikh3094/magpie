package in.aceventura.HansaMagpie;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.regex.Pattern;

import in.aceventura.HansaMagpie.NewMenus.ActivityDataEntry;
import in.aceventura.HansaMagpie.NewMenus.ActivityNotifications;
import in.aceventura.HansaMagpie.NewMenus.ActivityResetPassword;
import in.aceventura.HansaMagpie.NewMenus.ActivityStatus;
import in.aceventura.HansaMagpie.NewMenus.InterviewAllocationActivity;
import in.aceventura.HansaMagpie.NewMenus.ProfileDetailsActivity;

public class UserRegistrationActivity extends AppCompatActivity {

    EditText edtUId, edtMobile, edtEmail;
    Button btnRegister;
    CheckBox chkTC;
    boolean tcAcceptted;
    String respName, respMobile, respEmail;
    private ArrayList<AddStudentBean> addStudentBeansArraylist;
    private DBManager dbManager;
    private ProgressDialog progressDialog;
    private AddStudentBean addStudentBean;
    private String Intid;
    private RequestQueue requestQueue, registraionRequestQueue;
    SharedClass sharedClass;
    TextView txtIntvName;

    String loginUserID;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_registration);
        getIds();

        requestQueue = Volley.newRequestQueue(getBaseContext());
        registraionRequestQueue = Volley.newRequestQueue(getBaseContext());
        sharedClass = new SharedClass(UserRegistrationActivity.this);

        txtIntvName = findViewById(R.id.txtIntName);
        String intName = sharedClass.loadSharedPreference_IntName();
        txtIntvName.setText(intName);   //Setting interviewers name

        Intid = sharedClass.loadSharedPreference_UserId();
        //Interviewer details from Login Activity
        Intent loginIntent = getIntent();
        loginUserID = loginIntent.getStringExtra("userid");
        String pwd = loginIntent.getStringExtra("pwd");
        Log.e("INT", "Value=" + loginUserID);
        // TODO: 09-03-2020 Saving Today's Date in SharedPref
        autoLogOut();   //Logging out the user method on the basis if loggedIn date
        getVersion();   // checking version method

        //loginPwd = String.valueOf(getIntent().getStringExtra("pwd"));
        //checking if the user_id and pwd are not same
        if (Intid.equals(pwd)) {
            AlertDialog.Builder dialog = new AlertDialog.Builder(UserRegistrationActivity.this);
            dialog.setCancelable(false);
            dialog.setIcon(android.R.drawable.ic_dialog_alert);
            dialog.setTitle("Change Password");
            dialog.setMessage("Please Change the password");
            dialog.setPositiveButton("Change", (dialog1, id) -> {
                Intent i = new Intent(UserRegistrationActivity.this, ActivityResetPassword.class);
                UserRegistrationActivity.this.startActivity(i);
            });

            final AlertDialog alert = dialog.create();
            alert.show();

        }


        if (!SharedClass.getInstance(this).isLoggedIn()) {
            finish();
            startActivity(new Intent(this, LoginActivity.class));
        }

        addStudentBean = new AddStudentBean();
        progressDialog = new ProgressDialog(this);
        addStudentBeansArraylist = new ArrayList<>();

        dbManager = new DBManager(UserRegistrationActivity.this, "UserData.db", null, 1);
        addStudentBeansArraylist = dbManager.getAllMobileNo();

        //user is logged in or not...
        if (!SharedClass.getInstance(this).isLoggedIn()) {
            finish();
            startActivity(new Intent(this, LoginActivity.class));
        }

        btnRegister.setOnClickListener(v -> {

            if (validate()) {
                progressDialog.show();
                progressDialog.setMessage("Registering the respondent...");
                progressDialog.setCancelable(false);
                //checkbox is checked or not? if check send true
                if (chkTC.isChecked()) {
                    tcAcceptted = true;
                }

                //User's details
                respName = edtUId.getText().toString();
                respMobile = edtMobile.getText().toString();
                respEmail = edtEmail.getText().toString();
                String tcCheckbbox = String.valueOf(tcAcceptted);
                String RespondentRegistrationInfo = Config.PROJECT_URL + "RespondentRegistrationInfo";
                String Msg = "Respondent Registration Details saved successfully.";

                final HashMap<String, String> registrationParams = new HashMap<>();
                registrationParams.put("RespName", respName);
                registrationParams.put("RespMobile", respMobile);

                if (respEmail.isEmpty()) {
                    registrationParams.put("RespEmailId", "");
                } else {
                    registrationParams.put("RespEmailId", respEmail);

                }
                registrationParams.put("TermCondition", tcCheckbbox);
                registrationParams.put("UserId", Intid);

                JsonObjectRequest registrationJsonObjectRequest = new JsonObjectRequest(RespondentRegistrationInfo, new JSONObject(registrationParams), registrationResponse -> {
                    try {

                        if (registrationResponse.getString("Msg").equalsIgnoreCase(Msg)) {
                            Toast.makeText(this, "Respondent successfully register", Toast.LENGTH_SHORT).show();
                            Intent intent = new Intent(UserRegistrationActivity.this, MainActivity.class);
                            intent.putExtra("userid", loginUserID); //interviewer's id
                            intent.putExtra("respMobile", edtMobile.getText().toString()); //User's Mobile
                            intent.putExtra("pwd", pwd);//interviewer's pwd
                            startActivity(intent);
                            finish();

                        } else {
                            progressDialog.dismiss();
                            Toast.makeText(this, "Server Error: Unable to Register", Toast.LENGTH_SHORT).show();
                        }
                    } catch (JSONException e1) {
                        progressDialog.dismiss();
                        e1.printStackTrace();
                        System.out.println(e1.getMessage());
                    }

                }, Throwable::printStackTrace);

                registraionRequestQueue.add(registrationJsonObjectRequest);

            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        getVersion();
        autoLogOut();
    }

    private void autoLogOut() {
        String loggedInDate = sharedClass.getLoginDate(); //getting logged in date from shared pref

        //geting System date
        Date c = Calendar.getInstance().getTime();
        @SuppressLint("SimpleDateFormat") SimpleDateFormat sysSdf = new SimpleDateFormat("dd-MM-yyyy");
        String systemFormattedDate = sysSdf.format(c);

        //logout if condition is not satisfied
        if (!loggedInDate.equals(systemFormattedDate)) {
            AlertDialog.Builder dialog = new AlertDialog.Builder(UserRegistrationActivity.this);
            dialog.setIcon(android.R.drawable.ic_dialog_alert);
            dialog.setTitle("Session Expired");
            dialog.setCancelable(false);
            dialog.setMessage("Your session has expired.\nPlease Log in to continue.");
            dialog.setPositiveButton("Ok", (dialog1, which) -> logout_user());
            final AlertDialog alert = dialog.create();
            alert.show();
        }
    }

    //option menu
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.optioncomment, menu);
        return true;
    }

    //DataEntry Method Call
    private void data_entry() {
        Intent intent = new Intent(this, ActivityDataEntry.class);
        startActivity(intent);

    }

    //Notification Method Call
    private void notifications() {
        Intent intent = new Intent(this, ActivityNotifications.class);
        startActivity(intent);

    }

    //Status Method Call
    private void status() {
        Intent intent = new Intent(this, ActivityStatus.class);
        startActivity(intent);

    }

    //Reset Password Method Call
    private void resetPassword() {
        Intent intent = new Intent(this, ActivityResetPassword.class);
        startActivity(intent);
    }

    //Reset Password Method Call
    private void interviewerAllocation() {
        Intent intent = new Intent(this, InterviewAllocationActivity.class);
        intent.putExtra("userid", loginUserID);
        Log.e("INT", "ValuesUserReg" + loginUserID);
        Log.e("INT", "Shared Values" + SharedClass.getInstance(this).loadSharedPreference_UserId());
        startActivity(intent);
    }

    private void logout_user() {
        SharedClass.getInstance(this).logout();
        finish();
        startActivity(new Intent(this, LoginActivity.class));
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        //Data Entry menu

        if (id == R.id.data_entry) {
            data_entry();
            return true;
        }
        if (id == R.id.profiledetails) {
            profile_details();
            return true;
        }
        //Notification menu

        if (id == R.id.notifications) {
            notifications();
            return true;
        }

        //Status menu

        if (id == R.id.status) {
            status();
            return true;
        }

        //Reset Password
        if (id == R.id.reset) {
            resetPassword();
            return true;
        }

        //Reset Password
        if (id == R.id.interviewerAllocation) {
            interviewerAllocation();
            return true;
        }


        //Logout menu
        if (id == R.id.logout) {
            logout_user();
            // dbManager.delete();
            return true;
        }

        //Menu sync
        if (id == R.id.menuSync) {
            if (isNetworkConnected()) {
                otpVerifysync();
                return true;
            } else {
                Toast.makeText(this, "Connect Your Internet Connection", Toast.LENGTH_SHORT).show();
            }

        }
        return super.onOptionsItemSelected(item);
    }

    private void profile_details() {
        Intent intent = new Intent(this, ProfileDetailsActivity.class);
        startActivity(intent);


    }

    private void otpVerifysync() {

        addStudentBeansArraylist = dbManager.getAllMobileNo();
        String count = String.valueOf(addStudentBeansArraylist.size());

        progressDialog.setMessage("Verify URN...");
        progressDialog.setCanceledOnTouchOutside(false);

        progressDialog.show();
        if (addStudentBeansArraylist.size() > 0) {
            Intid = addStudentBeansArraylist.get(0).getInt_Id();
            String edtMobOtpno = addStudentBeansArraylist.get(0).getMobile_No();
            String edtotp = addStudentBeansArraylist.get(0).getOTP();

            HashMap<String, String> params = new HashMap<>();
            params.put("Int_Id", Intid);
            params.put("Mobile_No", edtMobOtpno);
            params.put("OTP", edtotp);

            String url = Config.PROJECT_URL + "VerifySMS";

            JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(url, new JSONObject(params), response -> {
                progressDialog.dismiss();
                String IsVerified = "True";

                try {
                    if (response.getString("IsVerified").equalsIgnoreCase(IsVerified)) {

                        setpojo1();
                        dbManager.update(addStudentBean);
                        addStudentBeansArraylist.remove(0);
                        if (addStudentBeansArraylist.size() > 0) {
                            new Handler().postDelayed(() -> otpVerifysync(), 5000);
                            Toast.makeText(UserRegistrationActivity.this, "URN verified successfully..", Toast.LENGTH_SHORT).show();

                        } else {
                            Toast.makeText(UserRegistrationActivity.this, "Data Sync Finished..", Toast.LENGTH_SHORT).show();
                        }


                    } else {

                        Toast.makeText(UserRegistrationActivity.this, "URN not verified..", Toast.LENGTH_SHORT).show();

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }


            }, error -> {

            });
            jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(
                    30000,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            requestQueue.add(jsonObjectRequest);
        } else {
            progressDialog.dismiss();
            Toast.makeText(this, "No Data to Sync..", Toast.LENGTH_SHORT).show();
        }
    }

    private void setpojo1() {
        addStudentBean = new AddStudentBean();
        addStudentBean.setMobile_No(addStudentBeansArraylist.get(0).getMobile_No());
        addStudentBean.setOTP(addStudentBeansArraylist.get(0).getOTP());
        addStudentBean.setIsSync("YES");
        addStudentBean.setInt_Id(addStudentBeansArraylist.get(0).getInt_Id());
        addStudentBean.setId(addStudentBeansArraylist.get(0).getId());
    }

    private boolean isNetworkConnected() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        return cm.getActiveNetworkInfo() != null;

    }

    private void getIds() {
        edtUId = findViewById(R.id.edtUIdRegistration);
        edtMobile = findViewById(R.id.edtMobileRegistration);
        edtEmail = findViewById(R.id.edtEmailRegistration);
        chkTC = findViewById(R.id.chkTCRegistration);
        btnRegister = findViewById(R.id.btnRegister);
    }

    public boolean validate() {
        boolean status = true;

        String email = edtEmail.getText().toString();
        String ed_Mobile = edtMobile.getText().toString().trim();
        String resName = edtUId.getText().toString().trim();

        if (resName.isEmpty()) {
            edtUId.setError("Respondent's Name cannot be empty");
            edtUId.setFocusable(true);
            status = false;
            //|| ed_Mobile.startsWith("5") || ed_Mobile.startsWith("4") || ed_Mobile.startsWith("3") || ed_Mobile.startsWith("2") || ed_Mobile.startsWith("1") || ed_Mobile.startsWith("0")
        } else if (ed_Mobile.isEmpty() || ed_Mobile.length() != 10 || ed_Mobile.length() == 0 || ed_Mobile.equals("") || ed_Mobile == null ) {
            edtMobile.setError("Enter valid Mobile number and cannot be empty");
            edtMobile.setFocusable(true);
            status = false;
        } else if (!chkTC.isChecked()) {
            chkTC.setError("Please accept the terms and conditions");
            status = false;
        } else if (!email.isEmpty()) {
            if (!isValidEmailId(edtEmail.getText().toString().trim())) {
                edtEmail.setError("Please enter valid Email id");
                status = false;
            }
        }

        return status;
    }

    private boolean isValidEmailId(String email) {

        return Pattern.compile("^(([\\w-]+\\.)+[\\w-]+|([a-zA-Z]{1}|[\\w-]{2,}))@"
                + "((([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
                + "[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\."
                + "([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
                + "[0-9]{1,2}|25[0-5]|2[0-4][0-9])){1}|"
                + "([a-zA-Z]+[\\w-]+\\.)+[a-zA-Z]{2,4})$").matcher(email).matches();
    }

    //Version Check Method
    private void getVersion() {
        String url = "http://203.115.122.103/SMSAPI/Service1.svc/Version";

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, url, new JSONObject(), response -> {

            progressDialog.dismiss();

            String androidversion;
            try {
                androidversion = response.getString("Version");
                if (!Config.LOCAL_ANDROID_VERSION.equals(androidversion)) {

                    AlertDialog.Builder dialog = new AlertDialog.Builder(UserRegistrationActivity.this);
                    dialog.setIcon(android.R.drawable.ic_dialog_alert);
                    dialog.setTitle(Config.LOCAL_ANDROID_VERSION_DAILOG_TITLE);
                    dialog.setCancelable(false);
                    dialog.setMessage(Config.LOCAL_ANDROID_VERSION_MESSAGE);
                    dialog.setPositiveButton("Update", (dialog1, id) -> UserRegistrationActivity.this.startActivity(new Intent(Intent.ACTION_VIEW,
                            Uri.parse("https://play.google.com/store/apps/details?id=in.aceventura.HansaMagpie"))));
                    final AlertDialog alert = dialog.create();
                    alert.show();
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }, error -> Toast.makeText(UserRegistrationActivity.this, "Error" + error.toString(), Toast.LENGTH_SHORT).show());
        jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(
                30000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(jsonObjectRequest);
    }
}
