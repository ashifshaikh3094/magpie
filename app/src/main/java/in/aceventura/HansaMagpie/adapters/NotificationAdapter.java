package in.aceventura.HansaMagpie.adapters;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import in.aceventura.HansaMagpie.R;
import in.aceventura.HansaMagpie.models.NotificationModel;

/**
 * {@link RecyclerView.Adapter} that can display a {@link } and makes a call to the
 * TODO: Replace the implementation with code for your data type.
 */
public  class NotificationAdapter extends RecyclerView.Adapter<NotificationAdapter.ViewHolder> {



    private List<NotificationModel> notiList;
    private Context context;
    public static String a;
    public static String b;


    public NotificationAdapter(Context context, List<NotificationModel> notiList) {
        this.notiList = notiList;
        this.context = context;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.notification_item_list, parent, false);
        return new ViewHolder(view);
    }


    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, int position) {
        holder.mNotiItem = notiList.get(position);
        holder.notiText.setText(holder.mNotiItem.getText());
        holder.notiType.setText(holder.mNotiItem.getType());


    }

    @Override
    public int getItemCount() {
        return notiList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private final View mView;
        private NotificationModel mNotiItem;
        @BindView(R.id.tv_notiType)
        TextView notiType;
        @BindView(R.id.tv_notiText)
        TextView notiText;
        ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
            mView = view;
        }

    }


}
