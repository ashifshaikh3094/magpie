package in.aceventura.HansaMagpie.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import in.aceventura.HansaMagpie.R;
import in.aceventura.HansaMagpie.models.NoSegmentModel;

public class NoSegmentAdapter extends RecyclerView.Adapter<NoSegmentAdapter.ViewHolder> {
    private List<NoSegmentModel> usersList_ori;
    private Context context;


    public NoSegmentAdapter(Context context, List<NoSegmentModel> list) {
        this.usersList_ori = list;
        this.context = context;
    }

    @NonNull
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view= LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.no_seg_item_list, viewGroup, false);
        return new NoSegmentAdapter.ViewHolder(view);
    }

    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {
        viewHolder.mItems1 = this.usersList_ori.get(i);
        viewHolder.tvTotal.setText(viewHolder.mItems1.getSegTotal());
        viewHolder.tvRejected.setText(viewHolder.mItems1.getSegRej());
    }

    public int getItemCount() {
        return this.usersList_ori.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        private NoSegmentModel mItems1;
        private final View mView;


        @BindView(R.id.textView2)
        TextView tvTotal;

        @BindView(R.id.textView3)
        TextView tvRejected;
        ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
            this.mView = view;
        }
    }

}
