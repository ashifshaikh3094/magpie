package in.aceventura.HansaMagpie.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

import in.aceventura.HansaMagpie.R;
import in.aceventura.HansaMagpie.models.StatusReportModel;

public class StatusReportAdapter extends RecyclerView.Adapter<StatusReportAdapter.MyHolder> {
    Context mContext;
    ArrayList<StatusReportModel> statusReportModels;

    public StatusReportAdapter(Context mContext, ArrayList<StatusReportModel> statusReportModels) {
        this.mContext = mContext;
        this.statusReportModels = statusReportModels;
    }

    @NonNull
    @Override
    public MyHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.status_report_item_list, parent, false);
        return new MyHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyHolder holder, int position) {
        StatusReportModel model = statusReportModels.get(position);
        holder.tv_PhoneNumber.setText(model.getPhone());
        holder.tv_Stuts.setText(model.getStatus());
    }
    @Override
    public int getItemCount() {
        return statusReportModels.size();
    }
    public class MyHolder extends RecyclerView.ViewHolder {
        TextView tv_PhoneNumber, tv_Stuts;
        public MyHolder(@NonNull View itemView) {
            super(itemView);
            tv_PhoneNumber = itemView.findViewById(R.id.tv_PhoneNumber);
            tv_Stuts = itemView.findViewById(R.id.tv_Stuts);
        }
    }
}
