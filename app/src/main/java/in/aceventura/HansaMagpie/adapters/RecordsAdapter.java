package in.aceventura.HansaMagpie.adapters;

import android.content.Context;
import android.graphics.Color;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import in.aceventura.HansaMagpie.R;
import in.aceventura.HansaMagpie.models.DataModel;

/**
 * {@link RecyclerView.Adapter} that can display a {@link } and makes a call to the
 * TODO: Replace the implementation with code for your data type.
 */
public class RecordsAdapter extends RecyclerView.Adapter<RecordsAdapter.ViewHolder> {



    private List<DataModel> usersList_ori;
    private Context context;
    private static final String TAG = "UsersListAdapter";


    public RecordsAdapter(Context context, List<DataModel> usersList_ori) {
        this.usersList_ori = usersList_ori;
        this.context = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.qcs_item_list, parent, false);
        return new ViewHolder(view);
    }


    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        holder.mItem = this.usersList_ori.get(position);
        holder.tvRecord.setText(holder.mItem.getRecord());
        switch (holder.mItem.getStatus()) {
            case "Rejected":
                holder.tvStatus.setText(holder.mItem.getStatus());
                holder.tvStatus.setTextColor(Color.RED);
                break;
            case "Rejected (J)":
                holder.tvStatus.setText(holder.mItem.getStatus());
                holder.tvStatus.setTextColor(Color.RED);
                break;
            case "TBC":
                holder.tvStatus.setText(holder.mItem.getStatus());
                holder.tvStatus.setTextColor(Color.BLUE);
                break;
            default:
                holder.tvStatus.setText(holder.mItem.getStatus());
                break;
        }

    }

    @Override
    public int getItemCount() {
        return usersList_ori.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private final View mView;
        private DataModel mItem;
        @BindView(R.id.textView)
        TextView tvRecord;
        @BindView(R.id.textView2)
        TextView tvStatus;
        ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
            mView = view;
        }

    }


}
