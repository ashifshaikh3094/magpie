package in.aceventura.HansaMagpie.adapters;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import in.aceventura.HansaMagpie.R;
import in.aceventura.HansaMagpie.models.SegmentModel;

public class SegmentAdapter extends RecyclerView.Adapter<SegmentAdapter.ViewHolder> {


    private List<SegmentModel> usersList_ori;
    private Context context;


    public SegmentAdapter(Context context, List<SegmentModel> usersList_ori) {
        this.usersList_ori = usersList_ori;
        this.context = context;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.seg_item_list, parent, false);
        return new SegmentAdapter.ViewHolder(view);
    }


    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, int position) {
        holder.mItems = usersList_ori.get(position);
        holder.tvSegment.setText(holder.mItems.getSegName());
        holder.tvTotal.setText(holder.mItems.getSegTotal());
        holder.tvRejected.setText(holder.mItems.getSegRej());


    }

    @Override
    public int getItemCount() {
        return usersList_ori.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private final View mView;
        private SegmentModel mItems;
        @BindView(R.id.textView)
        TextView tvSegment;
        @BindView(R.id.textView2)
        TextView tvTotal;
        @BindView(R.id.textView3)
        TextView tvRejected;
        ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
            mView = view;
        }

    }


}
