package in.aceventura.HansaMagpie;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TableLayout;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import in.aceventura.HansaMagpie.adapters.NoSegmentAdapter;
import in.aceventura.HansaMagpie.adapters.SegmentAdapter;
import in.aceventura.HansaMagpie.models.NoSegmentModel;
import in.aceventura.HansaMagpie.models.SegmentModel;

import static in.aceventura.HansaMagpie.ActivityQCS.CenterName;
import static in.aceventura.HansaMagpie.ActivityQCS.JobName;

public class ActivitySummary extends AppCompatActivity implements AdapterView.OnItemSelectedListener {
    private static final String TAG = null;
    Spinner ProjectSpinner, CentreSpinner, SegmentSpinner;
    TableLayout tableLayout;
    public List<SegmentModel> SegmentModelList = new ArrayList<>();
    public List<NoSegmentModel> NoSegmentModelList = new ArrayList<>();
    RecyclerView rvRecords1;


    public String pURL = Config.PROJECT_URL + "JobName";
    public String cURL = Config.PROJECT_URL + "CenterName";
    public String sURL = Config.PROJECT_URL + "SegmentName";
    public String sumURL = Config.PROJECT_URL + "SummaryOfInterview";


    List<String> Pnames = new ArrayList<>();
    List<String> Cnames = new ArrayList<>();
    List<String> Snames = new ArrayList<>();

    public RequestQueue mQueue1, mQueue2, mQueue3;
    public String job;
    ArrayList aList;
    ArrayList recList = new ArrayList<String>();

    ArrayAdapter<String> cAdapter;
    ArrayAdapter<String> sAdapter;
    ArrayAdapter<String> pAdapter;

    public String projectItem;
    public String CName;
    public String SName;
    public String uName;
    public String Records;
    ProgressDialog progressDialog;
    RecyclerView rvRecords;

    LinearLayout lData;
    LinearLayout lData1;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_summary);
        ProjectSpinner = findViewById(R.id.PSspinner);
        CentreSpinner = findViewById(R.id.CSspinner);
        SegmentSpinner = findViewById(R.id.SSspinner);
        tableLayout = findViewById(R.id.table);
        lData = findViewById(R.id.data);
        lData1 = findViewById(R.id.data1);

        rvRecords = findViewById(R.id.rv_records);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        rvRecords.setLayoutManager(layoutManager);
        rvRecords.setItemAnimator(new DefaultItemAnimator());

        rvRecords1 = findViewById(R.id.rv_records1);
        LinearLayoutManager layoutManager1 = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        rvRecords1.setLayoutManager(layoutManager1);
        rvRecords1.setItemAnimator(new DefaultItemAnimator());
        RequestQueue mQueue = Volley.newRequestQueue(this);
        mQueue1 = Volley.newRequestQueue(this);
        mQueue2 = Volley.newRequestQueue(this);
        mQueue3 = Volley.newRequestQueue(this);
        uName = SharedClass.getInstance(this).loadSharedPreference_UserId();
        progressDialog = new ProgressDialog(this);


        final HashMap<String, String> params = new HashMap<>();
        params.put("uname", uName);
        progressDialog.show();
        progressDialog.setMessage("Loading");
        progressDialog.setCanceledOnTouchOutside(false);


        JsonObjectRequest request = new JsonObjectRequest(pURL, new JSONObject(params), response -> {
            try {
                JobName = response.getString("JobName");
                ArrayList aList = new ArrayList<>(Arrays.asList(JobName.split(",")));

                for (int i = 0; i < aList.size(); i++) {
                    job = (String) aList.get(i);
                    if (!JobName.equals("null")) {
                        Pnames.add(job);
                        progressDialog.dismiss();
                    } else {
                        progressDialog.dismiss();
                        return;
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }, Throwable::printStackTrace);
        mQueue.add(request);
        //---------------------------------------------------------
        ProjectSpinner.setOnItemSelectedListener(this);
        Pnames.add(0, "Select JobName");
        pAdapter = new ArrayAdapter<>(this,
                android.R.layout.simple_spinner_dropdown_item, Pnames);
        pAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        ProjectSpinner.setAdapter(pAdapter);

        //---------------------------------------------------------
        CentreSpinner.setOnItemSelectedListener(this);
        Cnames.add(0, "Select Center");
        cAdapter = new ArrayAdapter<>(this,
                R.layout.support_simple_spinner_dropdown_item, Cnames);
        cAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        CentreSpinner.setAdapter(cAdapter);

        //----------------------------------------------------------
        SegmentSpinner.setOnItemSelectedListener(this);
        Snames.add(0, "Select Segment");
        sAdapter = new ArrayAdapter<>(this,
                R.layout.support_simple_spinner_dropdown_item, Snames);
        sAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        SegmentSpinner.setAdapter(sAdapter);
        //-----------------------------------------------------------

    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        if (parent.getId() == R.id.PSspinner) {

            if (parent.getItemAtPosition(position).equals("Select JobName")) {
                return;

            } else {
                cAdapter.clear();
                Cnames.add(0, "Select Center");
                projectItem = parent.getItemAtPosition(position).toString();
                cAdapter.notifyDataSetChanged();
                sAdapter.clear();
                Snames.add("Select Segment");
                CName = parent.getItemAtPosition(position).toString();
                sAdapter.notifyDataSetChanged();
                SegmentModelList.clear();
                getCenter(projectItem);

            }

        } else if (parent.getId() == R.id.CSspinner) {
            if (parent.getItemAtPosition(position).equals("Select Center")) {
                return;
            } else {
                sAdapter.clear();
                Snames.add(0, "Select Segment");
                Snames.add(1, "All");
                Snames.add(2, "No Segment");
                CName = parent.getItemAtPosition(position).toString();
                sAdapter.notifyDataSetChanged();
                getSegment();
            }
        } else if (parent.getId() == R.id.SSspinner) {
            if (parent.getItemAtPosition(position).equals("Select Segment")) {
                return;
            } else {
                SName = parent.getItemAtPosition(position).toString();
                getSummary(SName);
            }
        }


    }

    private void getSummary(String SName) {
        final HashMap<String, String> params3 = new HashMap<>();
        params3.put("uname", uName);
        params3.put("JobName", projectItem);
        params3.put("CenterName", CName);
        params3.put("SegmentName", SName);

        progressDialog.setMessage("Loading");
        progressDialog.show();
        progressDialog.setCanceledOnTouchOutside(false);

        JsonObjectRequest request3 = new JsonObjectRequest(sumURL, new JSONObject(params3),
                response -> {
                    System.out.println(response);
                    try {
                        JSONObject jsonObject = new JSONObject(response.toString());
                        if (jsonObject.getString("SegmentName").equals("No Segment")) {
                            lData1.setVisibility(View.VISIBLE);
                            lData.setVisibility(View.GONE);

                            NoSegmentModelList.clear();
                            if (jsonObject.has("Records")) {
                                String string = jsonObject.getString("Records");
                                System.out.println(string);
                                for (String split : string.split(",")) {
                                    String[] split2 = split.split(":");
                                    NoSegmentModelList.add(new NoSegmentModel(split2[1], split2[2]));
                                }
                                rvRecords1.setAdapter(new NoSegmentAdapter(getApplicationContext(), NoSegmentModelList));
                            } else {
                                Toast.makeText(this, "No records found", Toast.LENGTH_SHORT).show();
                            }
                        } else {
                            lData1.setVisibility(View.GONE);
                            lData.setVisibility(View.VISIBLE);

                            SegmentModelList.clear();
                            if (jsonObject.has("Records")) {
                                String string2 = jsonObject.getString("Records");
                                System.out.println(string2);
                                for (String split3 : string2.split(",")) {
                                    String[] split4 = split3.split(":");
                                    SegmentModelList.add(new SegmentModel(split4[0], split4[1], split4[2]));
                                }
                            } else {

                                Toast.makeText(this, "No records found", Toast.LENGTH_SHORT).show();
                            }
                            rvRecords.setAdapter(new SegmentAdapter(getApplicationContext(), SegmentModelList));
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    progressDialog.dismiss();
                }, error -> Toast.makeText(ActivitySummary.this, "", Toast.LENGTH_SHORT).show());
        mQueue3.add(request3);
        SegmentModelList.clear();
    }

    private void getSegment() {
        final HashMap<String, String> params2 = new HashMap<>();
        params2.put("uname", uName);
        params2.put("JobName", projectItem);
        progressDialog.show();
        progressDialog.setMessage("Loading");
        progressDialog.setCanceledOnTouchOutside(false);

        JsonObjectRequest request2 = new JsonObjectRequest(sURL, new JSONObject(params2),
                response -> {
                    try {
                        String SegmentName = response.getString("SegmentName");
                        aList = new ArrayList<>(Arrays.asList(SegmentName.split(",")));
                        for (int i = 0; i < aList.size(); i++) {
                            job = (String) aList.get(i);
                            Snames.add(job);
                            progressDialog.dismiss();
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }, error -> {

        });
        mQueue2.add(request2);
    }

    private void getCenter(String projectItem) {
        final HashMap<String, String> params1 = new HashMap<>();
        params1.put("uname", uName);
        params1.put("JobName", projectItem);
        progressDialog.show();
        progressDialog.setMessage("Loading");
        progressDialog.setCanceledOnTouchOutside(false);


        JsonObjectRequest request1 = new JsonObjectRequest(cURL, new JSONObject(params1),
                response -> {
                    try {
                        CenterName = response.getString("CenterName");
                        aList = new ArrayList<>(Arrays.asList(CenterName.split(",")));
                        for (int i = 0; i < aList.size(); i++) {
                            job = (String) aList.get(i);
                            Cnames.add(job);
                            progressDialog.dismiss();
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }, error -> {

        });
        mQueue1.add(request1);
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {
    }
}
