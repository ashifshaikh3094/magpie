package in.aceventura.HansaMagpie.NewMenus;

import java.io.Serializable;

class ClassPojo implements Serializable {
    String job;
    public ClassPojo(String job) {
        this.job =job;
    }

    public String getJob() {
        return job;
    }

    public void setJob(String job) {
        this.job = job;
    }

}
