package in.aceventura.HansaMagpie.NewMenus;

import android.app.DatePickerDialog;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.app.TaskStackBuilder;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import androidx.core.app.NotificationCompat;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Random;

import butterknife.BindView;
import in.aceventura.HansaMagpie.adapters.NotificationAdapter;
import in.aceventura.HansaMagpie.Config;
import in.aceventura.HansaMagpie.models.NotificationModel;
import in.aceventura.HansaMagpie.R;
import in.aceventura.HansaMagpie.SharedClass;

public class ActivityNotifications extends AppCompatActivity {

    public String notification = Config.PROJECT_URL + "Notification";

    RequestQueue mQueue;
    String uName;
    public static List<NotificationModel> NotiModelList = new ArrayList<>();
    ProgressDialog progressDialog;
    @BindView(R.id.tv_notiType)
    TextView notiType;
    @BindView(R.id.tv_notiText)
    TextView notiText;
    @BindView(R.id.datefilter)
    TextView datefilter;
    @BindView(R.id.rv_records)
    RecyclerView rv_notifications;
    int notisize;
    public static int numItems;
    public static int pos;
    String type = "All";
    Button btnToday;
    String nType,nText;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notifications);

        uName= SharedClass.getInstance(this).loadSharedPreference_UserId();
        mQueue= Volley.newRequestQueue(this);
        progressDialog=new ProgressDialog(this);
        btnToday = findViewById(R.id.btnToday);
        btnToday.setTextColor(getResources().getColor(R.color.BtnTextColor));

        rv_notifications=findViewById(R.id.rv_notifications);
        notiType=findViewById(R.id.tv_notiType);
        notiText=findViewById(R.id.tv_notiText);
        datefilter=findViewById(R.id.datefilter);

        LinearLayoutManager layoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        layoutManager.setSmoothScrollbarEnabled(true);
        rv_notifications.setLayoutManager(layoutManager);
        rv_notifications.setItemAnimator(new DefaultItemAnimator());
        rv_notifications.setAdapter(new NotificationAdapter(getApplicationContext(), NotiModelList));




        //call the api directly onClick of "Today" Btn
        btnToday.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                datefilter.setText("");
                progressDialog.show();
                NotiModelList.clear();
                final HashMap<String, String> params = new HashMap<>();
                params.put("uname", uName);
                params.put("type", type);

                progressDialog.setMessage("Loading");
                progressDialog.setCanceledOnTouchOutside(false);

                JsonObjectRequest request = new JsonObjectRequest(notification, new JSONObject(params), response -> {
                    progressDialog.dismiss();
                    String sampleArrStr;
                    try {
                        sampleArrStr = response.getString("Records");

                        if (sampleArrStr != null) {
                            String[] splitStr1 = sampleArrStr.split(",");
                            for (String s : splitStr1) {
                                String[] splitStr2 = s.split(":");
                                try {
                                    NotiModelList.add(new NotificationModel(splitStr2[0], splitStr2[1]));
                                    NotiModelList.notify();
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }
                            for (int j = 0;j<NotiModelList.size();j++){
                                nType = NotiModelList.get(j).getType();
                                nText = NotiModelList.get(j).getText();
                                sendnotification(nType,nText);
                            }
                        } else {
                            Toast.makeText(ActivityNotifications.this, "No Notifications", Toast.LENGTH_SHORT).show();
                        }

                        //check the size of notification list
                        notisize = NotiModelList.size();

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    rv_notifications.setAdapter(new NotificationAdapter(ActivityNotifications.this.getApplicationContext(), NotiModelList));

                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                });
                mQueue.add(request);
            }
        });

        final Calendar myCalendar = Calendar.getInstance();

        DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {
                // TODO Auto-generated method stub
                myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                myCalendar.set(Calendar.MONTH, monthOfYear);
                myCalendar.set(Calendar.YEAR, year);

                updateLabel();
            }


            //Get notifications of particular date...
            private void updateLabel() {

                datefilter.clearComposingText();
                String myFormat = "dd/MM/yyyy"; //In which you need put here
                SimpleDateFormat sdf = new SimpleDateFormat(myFormat);

                datefilter.setText(sdf.format(myCalendar.getTime()));
                String fDate = datefilter.getText().toString();
                NotiModelList.clear();
                final HashMap<String, String> params = new HashMap<>();
                params.put("uname",uName);
                params.put("CreatedOn",fDate);


                progressDialog.show();
                progressDialog.setMessage("Loading");
                progressDialog.setCanceledOnTouchOutside(false);

                JsonObjectRequest request = new JsonObjectRequest(notification, new JSONObject(params), response -> {
                    progressDialog.dismiss();
                    String sampleArrStr;
                    try {
                        sampleArrStr = response.getString("Records");

                        if (sampleArrStr !=null){
                            String[] splitStr1 = sampleArrStr.split(",");
                            for (String s : splitStr1) {
                                String[] splitStr2 = s.split(":");
                                try {
                                    NotiModelList.add(new NotificationModel(splitStr2[0], splitStr2[1]));
                                    NotiModelList.notify();
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }
                        }else {
                            Toast.makeText(ActivityNotifications.this, "", Toast.LENGTH_SHORT).show();
                        }

                        //check the size of notification list
                        notisize = NotiModelList.size();

                    }
                    catch (JSONException e) {
                        e.printStackTrace();
                    }
                    rv_notifications.setAdapter(new NotificationAdapter(getApplicationContext(), NotiModelList));

                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                });mQueue.add(request);

            }

        };

//      clear the list of notification everytym before new items....
        NotiModelList.clear();
        final HashMap<String, String> params = new HashMap<>();
        params.put("uname",uName);
        params.put("type",type);
        progressDialog.show();
        progressDialog.setMessage("Loading");
        progressDialog.setCanceledOnTouchOutside(false);

        //on opening of activity call the api directly
        JsonObjectRequest request = new JsonObjectRequest(notification, new JSONObject(params), new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                progressDialog.dismiss();
                String sampleArrStr;
                try {
                    sampleArrStr = response.getString("Records");

                    if (sampleArrStr != null) {
                        String[] splitStr1 = sampleArrStr.split(",");
                        for (String s : splitStr1) {
                            String[] splitStr2 = s.split(":");

                            try {
                                NotiModelList.add(new NotificationModel(splitStr2[0], splitStr2[1]));
                                NotiModelList.notify();

                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                        for (int i = 0;i<NotiModelList.size();i++){
                           nType = NotiModelList.get(i).getType();
                           nText = NotiModelList.get(i).getText();
                            sendnotification(nType,nText);
                        }

                    } else {
                        Toast.makeText(ActivityNotifications.this, "No Notifications", Toast.LENGTH_SHORT).show();
                    }

                    //check the size of notification list
                    notisize = NotiModelList.size();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                rv_notifications.setAdapter(new NotificationAdapter(ActivityNotifications.this.getApplicationContext(), NotiModelList));

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });mQueue.add(request);



        datefilter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                datefilter.setText("");
                new DatePickerDialog(ActivityNotifications.this, date,
                        myCalendar.get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                        myCalendar.get(Calendar.DAY_OF_MONTH)).show();

            }
        });
    }
    @Override
    protected void onPause() {
        final HashMap<String, String> params = new HashMap<>();
        params.put("uname",uName);
        params.put("type",type);
        progressDialog.show();
        progressDialog.setMessage("Loading");
        progressDialog.setCanceledOnTouchOutside(false);

        //on opening of activity call the api directly
        JsonObjectRequest request = new JsonObjectRequest(notification, new JSONObject(params), new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
//            Toast.makeText(this, ""+response, Toast.LENGTH_SHORT).show();
                progressDialog.dismiss();
                String sampleArrStr;
                try {
                    sampleArrStr = response.getString("Records");

                    if (sampleArrStr != null) {
                        String[] splitStr1 = sampleArrStr.split(",");
                        for (String s : splitStr1) {
                            String[] splitStr2 = s.split(":");

                            try {
                                NotiModelList.add(new NotificationModel(splitStr2[0], splitStr2[1]));
                                NotiModelList.notify();

                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                        for (int i = 0;i<NotiModelList.size();i++){
                            nType = NotiModelList.get(i).getType();
                            nText = NotiModelList.get(i).getText();
                            sendnotification(nType,nText);
                        }

                    } else {
                        Toast.makeText(ActivityNotifications.this, "No Notifications", Toast.LENGTH_SHORT).show();
                    }

                    //check the size of notification list
                    notisize = NotiModelList.size();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                rv_notifications.setAdapter(new NotificationAdapter(ActivityNotifications.this.getApplicationContext(), NotiModelList));

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });mQueue.add(request);


        super.onPause();
    }
    private void sendnotification(String nType, String nText) {
        Random random = new Random();
        int m = random.nextInt(9999 - 1000);
        NotificationCompat.Builder mBuilder =new NotificationCompat.Builder(ActivityNotifications.this)
                .setTicker("MNV")
                .setContentTitle(nType)
                .setContentText(nText)
                .setSmallIcon(R.drawable.hansalogo);
        Intent intent1 = new Intent(this, ActivityNotifications.class);
        TaskStackBuilder stackBuilder = null;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.JELLY_BEAN) {
            stackBuilder = TaskStackBuilder.create(this);
            stackBuilder.addParentStack(ActivityNotifications.class);
            stackBuilder.addNextIntent(intent1);
            PendingIntent resultPendingIntent = stackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT);
            mBuilder.setContentIntent(resultPendingIntent);
            NotificationManager mNotificationManager = (NotificationManager)getSystemService(Context.NOTIFICATION_SERVICE);
            mBuilder.setDefaults(Notification.DEFAULT_SOUND | Notification.DEFAULT_VIBRATE);
            mBuilder.setAutoCancel(true);
            if (mNotificationManager != null) {
                mNotificationManager.notify(m, mBuilder.build());
            }

        }
    }
}


