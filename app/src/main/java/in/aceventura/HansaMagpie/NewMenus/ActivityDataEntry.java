package in.aceventura.HansaMagpie.NewMenus;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import in.aceventura.HansaMagpie.R;
import in.aceventura.HansaMagpie.SharedClass;
import in.aceventura.HansaMagpie.Config;

public class ActivityDataEntry extends AppCompatActivity implements AdapterView.OnItemSelectedListener {

    public String pURL = Config.PROJECT_URL + "JobNameforDataEntry";
    public String cURL = Config.PROJECT_URL + "CenterNameforDataEntry";// //CenterName
    public String intDataURL = Config.PROJECT_URL + "IntData";


    public static String JobName, CenterName, FieldCen, FieldOff, JobNo, Team;
    public RequestQueue mQueue1, mQueue2, mQueue3;
    public String projectItem, uName, CName;

    Spinner ProjectspinnerDE, CentrespinnerDE;
    List<String> Pnames = new ArrayList<>();
    List<String> Cnames = new ArrayList<>();
    ArrayList aList;
    ProgressDialog progressDialog;
    ArrayAdapter<String> cAdapter;
    Button btnSubmit;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_data_entry);

        ProjectspinnerDE = findViewById(R.id.Pspinner);
        CentrespinnerDE = findViewById(R.id.Cspinner);
        RequestQueue mQueue = Volley.newRequestQueue(this);
        mQueue1 = Volley.newRequestQueue(this);
        mQueue2 = Volley.newRequestQueue(this);
        mQueue3 = Volley.newRequestQueue(this);
        progressDialog = new ProgressDialog(this);
        btnSubmit = findViewById(R.id.btnSubmit);
        uName = SharedClass.getInstance(this).loadSharedPreference_UserId();

        //======================================================================
        /*JobName Api*/

        final HashMap<String, String> params = new HashMap<>();
        params.put("uname", uName);
        progressDialog.show();
        progressDialog.setMessage("Loading");
        progressDialog.setCanceledOnTouchOutside(false);


        JsonObjectRequest request = new JsonObjectRequest(pURL, new JSONObject(params), response -> {
            try {
                JobName = response.getString("JobName");
                ArrayList aList = new ArrayList<>(Arrays.asList(JobName.split(",")));

                for (int i = 0; i < aList.size(); i++) {
                    String job = (String) aList.get(i);
                    if (!JobName.equals("null")) {
                        Pnames.add(job);
                        progressDialog.dismiss();
                    } else {
                        progressDialog.dismiss();
                        return;
                    }
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }

        }, Throwable::printStackTrace);
        mQueue.add(request);

        //======================================================================

        ProjectspinnerDE.setOnItemSelectedListener(this);
        Pnames.add(0, "Select JobName");
        ArrayAdapter<String> pAdapter = new ArrayAdapter<>(this,
                android.R.layout.simple_spinner_dropdown_item, Pnames);
        pAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        pAdapter.notifyDataSetChanged();
        ProjectspinnerDE.setAdapter(pAdapter);


        //------------------------

        CentrespinnerDE.setOnItemSelectedListener(this);
        Cnames.add(0, "Select Center");
        cAdapter = new ArrayAdapter<>(this,
                R.layout.support_simple_spinner_dropdown_item, Cnames);
        CentrespinnerDE.setAdapter(cAdapter);

        //Submit Button
        btnSubmit.setOnClickListener(v -> btn_submit(projectItem, CName));

    }

    private void btn_submit(String projectItem, String CName) {
        //get the respective data that will be shown on next page....

        final HashMap<String, String> params1 = new HashMap<>();
        params1.put("JobName", projectItem);
        params1.put("FieldCen", CName);
        progressDialog.show();
        progressDialog.setMessage("Loading");
        progressDialog.setCanceledOnTouchOutside(false);


        JsonObjectRequest request3 = new JsonObjectRequest(intDataURL, new JSONObject(params1),
                response -> {
                    //code for details here..
                    if (response != null) {
                        progressDialog.dismiss();
                        try {
                            FieldCen = response.getString("FieldCen");
                            FieldOff = response.getString("FieldOff");
                            JobName = response.getString("JobName");
                            JobNo = response.getString("JobNo");
                            Team = response.getString("Team");
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        Intent intent = new Intent(ActivityDataEntry.this, DataEntrySection.class);
                        intent.putExtra("FieldCen", FieldCen);
                        intent.putExtra("FieldOff", FieldOff);
                        intent.putExtra("JobName", JobName);
                        intent.putExtra("JobNo", JobNo);
                        intent.putExtra("Team", Team);
                        startActivity(intent);
                    } else {
                        progressDialog.dismiss();
                        Toast.makeText(ActivityDataEntry.this, "", Toast.LENGTH_SHORT).show();

                    }
                }, Throwable::printStackTrace);
        mQueue3.add(request3);

    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        if (parent.getId() == R.id.Pspinner) {
            if (parent.getItemAtPosition(position).equals("Select JobName")) {
                return;
            } else {
                cAdapter.clear();
                Cnames.add(0, "Select Center");
                projectItem = parent.getItemAtPosition(position).toString();
                cAdapter.notifyDataSetChanged();
                getCenter(projectItem);
            }

        } else if (parent.getId() == R.id.Cspinner) {
            if (parent.getItemAtPosition(position).equals("Select Center")) {
                return;
            } else {
                CName = parent.getItemAtPosition(position).toString();
            }
        }
    }


    private void getCenter(String projectItem) {
        // Cnames.clear();
        final HashMap<String, String> params = new HashMap<>();
        params.put("uname", uName);
        params.put("JobName", projectItem);
        progressDialog.show();
        progressDialog.setMessage("Loading");
        progressDialog.setCanceledOnTouchOutside(false);

        //Toast.makeText(this, ""+params, Toast.LENGTH_SHORT).show();
        JsonObjectRequest request1 = new JsonObjectRequest(cURL, new JSONObject(params),
                response -> {
                    //Toast.makeText(ActivityQCS.this, ""+response, Toast.LENGTH_SHORT).show();
                    try {
                        //clear fn
                        //Cnames.clear();
                        CenterName = response.getString("CenterName");
                        aList = new ArrayList<>(Arrays.asList(CenterName.split(",")));
                        for (int i = 0; i < aList.size(); i++) {
                            String job = (String) aList.get(i);
                            Cnames.add(job);
                            progressDialog.dismiss();
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }, Throwable::printStackTrace);
        mQueue1.add(request1);
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }
}
