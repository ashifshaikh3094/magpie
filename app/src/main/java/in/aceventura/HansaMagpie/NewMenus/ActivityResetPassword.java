package in.aceventura.HansaMagpie.NewMenus;

import android.content.Intent;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;

import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

import in.aceventura.HansaMagpie.LoginActivity;
import in.aceventura.HansaMagpie.R;
import in.aceventura.HansaMagpie.SharedClass;
import in.aceventura.HansaMagpie.Config;

public class ActivityResetPassword extends AppCompatActivity {
    private EditText etNP, ETCP;
    Button btnCP;
    public static String cPassword;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reset_password);

        final RequestQueue mQueue = Volley.newRequestQueue(this);


        ETCP = findViewById(R.id.etCP);
        etNP = findViewById(R.id.etNP);
        btnCP = findViewById(R.id.btnCP);

        //get Username from SharesPreferences
        final String uName = SharedClass.getInstance(this).loadSharedPreference_UserId();

        final String url = Config.PROJECT_URL + "chngpass";


        btnCP.setOnClickListener(v -> {
            cPassword = ETCP.getText().toString().trim();

            final HashMap<String, String> params = new HashMap<>();
            params.put("uname", uName);
            params.put("pwd", cPassword);
            if (ETCP.getText().toString().equals(etNP.getText().toString())) {

                //-------------------JSON---------------------------------------
                JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(url,
                        new JSONObject(params), response -> {
                    String isUpdated = "True";

                    try {
                        if (response.getString("IsUpdated").equalsIgnoreCase(isUpdated)) {

                            Toast.makeText(ActivityResetPassword.this, "Password Changed !!", Toast.LENGTH_SHORT).show();
                            //logout user after resetting password...
                            logout_user();

                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }


                }, error -> {

                });
                mQueue.add(jsonObjectRequest);
            } else {
                Toast.makeText(ActivityResetPassword.this, "New Password and Confirm Password doesn't match", Toast.LENGTH_SHORT).show();
            }

        });


    }

    private void logout_user() {
        SharedClass.getInstance(this).logout();
        finish();
        startActivity(new Intent(this, LoginActivity.class));
    }
}
