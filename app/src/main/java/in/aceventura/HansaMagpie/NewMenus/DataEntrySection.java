package in.aceventura.HansaMagpie.NewMenus;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.util.Patterns;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.regex.Pattern;

import in.aceventura.HansaMagpie.Config;
import in.aceventura.HansaMagpie.R;
import in.aceventura.HansaMagpie.SharedClass;

public class DataEntrySection extends AppCompatActivity implements AdapterView.OnItemSelectedListener {

    public String sURL = Config.PROJECT_URL + "SegmentName";
    public String IntName = Config.PROJECT_URL + "IntName";
    public String SupName = Config.PROJECT_URL + "SupName";
    public String SupID = Config.PROJECT_URL + "SupID";
    public String EicID = Config.PROJECT_URL + "EicId";
    public String EicName = Config.PROJECT_URL + "EicName";
    public String RespProfileSheetData = Config.PROJECT_URL + "RespProfileSheetData";
    public String MobileNoData = Config.PROJECT_URL + "MobileNoData";
    public String OTPVerifyData = Config.PROJECT_URL + "OTPVerifyData";
    public String SaveInterviewData = Config.PROJECT_URL + "SaveInterviewData";

    String doi = "";
    public RequestQueue mQueue, mQueue1, mQueue2, mQueue3, mQueue4, mQueue5, mQueue6, mQueue7, mQueue8, mQueue9;
    public String SName, IntId, SupId, EicId, typeQC;
    public LinearLayout linearLayout16, linearLayout19, dynamic_layout;
    public String job;
    Button SubmitEntry;
    TextView job_name, job_no, field_center, field_office, date_of_interview, int_name, sup_name, eic_name;
    EditText respondent_tel, urn, respondent_name, pincode, availableEmail;
    String FieldCen, FieldOff, JobName, JobNo, Team, uName, email_check;
    Spinner Segmentspinner, IntIdspinner, QCspinner, SupIdspinner, EicIdspinner;
    List<String> Snames = new ArrayList<>();
    List<String> IntIds = new ArrayList<>();
    List<String> TypesQC = new ArrayList<>();
    List<String> SupIds = new ArrayList<>();
    List<String> EicIds = new ArrayList<>();
    CheckBox chk_superviser, chk_auditor, chk_eic, chk_manager;
    ArrayAdapter<String> sAdapter;
    ArrayAdapter<String> intIdsAdapter;
    ArrayAdapter<String> supIdsAdapter;
    ArrayAdapter<String> eicIdsAdapter;
    ArrayAdapter<String> QCAdapter;
    DatePickerDialog picker;
    ProgressDialog progressDialog;
    ArrayList aList;
    String otp_matched;
    String Header;
    String ansCB;
    ArrayList<String> selectedCheckboxes;
    List<View> allViewInstance = new ArrayList<>();
    List<EditText> allEds = new ArrayList<>();
    private RadioButton email, email_yes, email_no;
    JSONObject jObjRequestProfileSheetData = new JSONObject();
    RadioGroup email_availability;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_data_entry_section);
        progressDialog = new ProgressDialog(this);
        Segmentspinner = findViewById(R.id.segment_spinner);
        IntIdspinner = findViewById(R.id.intId_spinner);
        SupIdspinner = findViewById(R.id.supId_spinner);
        EicIdspinner = findViewById(R.id.eicIdspinner);
        QCspinner = findViewById(R.id.types_of_qc);
        respondent_tel = findViewById(R.id.respondent_tel);
        int_name = findViewById(R.id.int_name);
        sup_name = findViewById(R.id.sup_name);
        eic_name = findViewById(R.id.eic_name);
        respondent_name = findViewById(R.id.respondent_name);
        urn = findViewById(R.id.urn);
        pincode = findViewById(R.id.pincode);
        SubmitEntry = findViewById(R.id.submit_interviewer_data_entry);
        chk_superviser = findViewById(R.id.chk_superviser);
        chk_auditor = findViewById(R.id.chk_auditor);
        chk_eic = findViewById(R.id.chk_eic);
        chk_manager = findViewById(R.id.chk_manager);
        email_yes = findViewById(R.id.email_yes);
        email_no = findViewById(R.id.email_no);
        email_availability = findViewById(R.id.email_availability);
        availableEmail = findViewById(R.id.availableEmail);
        availableEmail.setVisibility(View.GONE);

        mQueue = Volley.newRequestQueue(this);
        mQueue1 = Volley.newRequestQueue(this);
        mQueue2 = Volley.newRequestQueue(this);
        mQueue3 = Volley.newRequestQueue(this);
        mQueue4 = Volley.newRequestQueue(this);
        mQueue5 = Volley.newRequestQueue(this);
        mQueue6 = Volley.newRequestQueue(this);
        mQueue7 = Volley.newRequestQueue(this);
        mQueue8 = Volley.newRequestQueue(this);
        mQueue9 = Volley.newRequestQueue(this);

        linearLayout19 = findViewById(R.id.linearLayout19);
        linearLayout16 = findViewById(R.id.linearLayout16);
        dynamic_layout = findViewById(R.id.dynamic_layout);
        linearLayout19.setVisibility(View.GONE);
        linearLayout16.setVisibility(View.GONE);

        field_office = findViewById(R.id.field_office);
        field_center = findViewById(R.id.field_center);
        job_no = findViewById(R.id.job_no);
        job_name = findViewById(R.id.job_name);
        date_of_interview = findViewById(R.id.date_of_interview);

        Segmentspinner.setOnItemSelectedListener(DataEntrySection.this);
        Snames.add(0, "Select Segment");
        sAdapter = new ArrayAdapter<>(this,
                R.layout.support_simple_spinner_dropdown_item, Snames);
        sAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        Segmentspinner.setAdapter(sAdapter);

        SupIdspinner.setOnItemSelectedListener(DataEntrySection.this);
        SupIds.add(0, "Select Sup Id");
        SupIds.add(1, "0");
        supIdsAdapter = new ArrayAdapter<>(this,
                R.layout.support_simple_spinner_dropdown_item, SupIds);
        supIdsAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        SupIdspinner.setAdapter(supIdsAdapter);

        EicIdspinner.setOnItemSelectedListener(DataEntrySection.this);
        EicIds.add(0, "Select Eic Id");
        eicIdsAdapter = new ArrayAdapter<>(this,
                R.layout.support_simple_spinner_dropdown_item, EicIds);
        eicIdsAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        EicIdspinner.setAdapter(eicIdsAdapter);

        Intent in = getIntent();
        FieldCen = in.getStringExtra("FieldCen");
        FieldOff = in.getStringExtra("FieldOff");
        JobName = in.getStringExtra("JobName");
        JobNo = in.getStringExtra("JobNo");
        Team = in.getStringExtra("Team");


        //values to text field...
        field_center.setText(FieldCen);
        field_office.setText(FieldOff);
        job_name.setText(JobName);
        job_no.setText(JobNo);


        uName = SharedClass.getInstance(this).loadSharedPreference_UserId();
        getSegment(JobName, FieldCen);
        getIntId(uName);
        getTypesQC();
        setResTel();
        getSupId(JobNo, FieldOff);
        getEicId(JobNo, FieldOff);
        verifyOTP();
//      RespProfileSheetData(JobNo);


        final HashMap<String, String> params = new HashMap<>();
        params.put("JobNo", JobNo);
        progressDialog.show();
        progressDialog.setMessage("Loading");
        progressDialog.setCanceledOnTouchOutside(false);

        JsonObjectRequest request6 = new JsonObjectRequest(RespProfileSheetData, new JSONObject(params),
                response -> {

                    if (response != null) {
                        dynamic_layout.setVisibility(View.VISIBLE);
                        progressDialog.dismiss();
                        try {
                            //Displaying question and answers...
                            JSONObject jsonObject = new JSONObject(response.toString());
                            String res = jsonObject.getString("RespProfileSheetData");
                            JSONObject jsonObject1 = new JSONObject(res);
                            for (int i = 0; i < 50; i++) {
                                Header = "Header" + (i + 1);
                                if (jsonObject1.toString().contains(Header)) {
                                    TextView textView = new TextView(DataEntrySection.this);
                                    textView.setText(jsonObject1.optString(Header));
                                    textView.setTextSize(20);
                                    textView.setTextColor(Color.BLACK);
                                    allViewInstance.add(textView);
                                    dynamic_layout.addView(textView);
                                    String AnsType = "AnsType" + (i + 1);
                                    if (jsonObject1.toString().contains(AnsType)) {
                                        String anstype = jsonObject1.optString(AnsType);
                                        switch (anstype) {
                                            case "1":
                                                String AnsOption = "AnsOption" + (i + 1);
                                                RadioGroup rg = new RadioGroup(DataEntrySection.this); //create the RadioGroup
                                                rg.setOrientation(RadioGroup.VERTICAL);// RadioGroup.VERTICAL
                                                rg.setId(i + 1);
                                                rg.setTag(Header);
                                                if (jsonObject1.toString().contains(AnsOption)) {
                                                    String Ansopt = jsonObject1.getString(AnsOption);
                                                    ArrayList aList = new ArrayList<>(Arrays.asList(Ansopt.split(",")));

                                                    for (int j = 0; j < aList.size(); j++) {
                                                        RadioButton rb = new RadioButton(DataEntrySection.this);
                                                        String job = (String) aList.get(j);
                                                        rb.setText(job);
                                                        rb.setTextSize(18);
                                                        rb.setId(j + 100);
                                                        rg.addView(rb);
                                                    }
                                                }
                                                allViewInstance.add(rg);

                                                rg.setOnCheckedChangeListener((group, checkedId) -> {

                                                    RadioButton rbChecked = group.findViewById(checkedId);
                                                    try {
                                                        jObjRequestProfileSheetData.put(rg.getTag().toString(), rbChecked.getText());
                                                    } catch (JSONException e) {
                                                        e.printStackTrace();
                                                    }
                                                });
                                                dynamic_layout.addView(rg);
                                                break;

                                            case "2":
                                                String AnsOption1 = "AnsOption" + (i + 1);
                                                selectedCheckboxes = new ArrayList<>();
                                                if (jsonObject1.toString().contains(AnsOption1)) {
                                                    String Ansopt = jsonObject1.getString(AnsOption1);
                                                    ArrayList aList = new ArrayList<>(Arrays.asList(Ansopt.split(",")));
                                                    int j;
                                                    for (j = 0; j < aList.size(); j++) {
                                                        CheckBox cbOptions = new CheckBox(DataEntrySection.this);
                                                        String job = (String) aList.get(j);
                                                        cbOptions.setText(job);
                                                        cbOptions.setTag(Header + "-" + job);
                                                        cbOptions.setChecked(false);
                                                        cbOptions.setTextColor(Color.BLACK);
                                                        cbOptions.setTextSize(18f);
                                                        dynamic_layout.addView(cbOptions);

                                                        HashMap<String, Set<String>> hmCbAnswer = new HashMap();
                                                        hmCbAnswer.put(Header, new HashSet<>());

                                                        cbOptions.setOnCheckedChangeListener((buttonView, isChecked) -> {
                                                            String objHeader = buttonView.getTag().toString().replace("-" + buttonView.getText().toString(), "");

                                                            if (isChecked) {
                                                                selectedCheckboxes.add(/*"'" +*/ buttonView.getText().toString()/* + "'"*/);
                                                            } else {
                                                                selectedCheckboxes.remove(/*"'" + */buttonView.getText().toString()/* + "'"*/);
                                                            }
                                                            String ans1 = selectedCheckboxes.toString().replace("[", "");
                                                            ansCB = ans1.replace("]", "");
                                                            try {
                                                                jObjRequestProfileSheetData.put(objHeader, ansCB);
                                                            } catch (JSONException e) {
                                                                e.printStackTrace();
                                                            }
                                                        });
                                                    }
                                                }
                                                break;
                                            case "3":
                                                EditText editText = new EditText(DataEntrySection.this);
                                                editText.setTextSize(18);
                                                editText.setId(i + 1);
                                                editText.setTag(Header);
                                                allEds.add(editText);
                                                allViewInstance.add(editText);
                                                dynamic_layout.addView(editText);

                                                editText.addTextChangedListener(new TextWatcher() {
                                                    @Override
                                                    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                                                    }

                                                    @Override
                                                    public void onTextChanged(CharSequence s, int start, int before, int count) {
                                                        try {
                                                            jObjRequestProfileSheetData.put(editText.getTag().toString(), s.toString());
                                                        } catch (JSONException e) {
                                                            e.printStackTrace();
                                                        }
                                                    }

                                                    @Override
                                                    public void afterTextChanged(Editable s) {

                                                    }
                                                });
                                                break;
                                        }
                                    }
                                }
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    } else {
                        progressDialog.dismiss();
                        dynamic_layout.setVisibility(View.GONE);
                    }
                }, error -> progressDialog.dismiss());
        mQueue6.add(request6);

        date_of_interview.setOnClickListener(v -> setDOI());

        email_availability.setOnCheckedChangeListener((group, checkedId) -> {
            email = group.findViewById(checkedId);
            email_check = email.getText().toString(); //email_check = Yes / No
            // TODO: 30-07-2020
            if (email_check.equals("Yes")) {
                availableEmail.setVisibility(View.VISIBLE);
            } else {
                availableEmail.setVisibility(View.GONE);
            }
        });

        SubmitEntry.setOnClickListener(v -> {
            String val = availableEmail.getText().toString(); //email
            int rId = email_availability.getCheckedRadioButtonId();
            String noneQC = QCspinner.getSelectedItem().toString();

            if (Segmentspinner.getSelectedItem().toString().equals("Select Segment")) {
                Toast.makeText(DataEntrySection.this, "Select Segment", Toast.LENGTH_SHORT).show();
            } else if (IntIdspinner.getSelectedItem().toString().equals("Select Int Id")) {
                Toast.makeText(getApplicationContext(), "Select Interviewer Id", Toast.LENGTH_SHORT).show();
            } else if (SupIdspinner.getSelectedItem().toString().equals("Select Sup Id")) {
                Toast.makeText(getApplicationContext(), "Select Sup Id", Toast.LENGTH_SHORT).show();
            } else if (EicIdspinner.getSelectedItem().toString().equals("Select Eic Id")) {
                Toast.makeText(getApplicationContext(), "Select EIC Id", Toast.LENGTH_SHORT).show();
            } else if (date_of_interview.getText().toString().length() == 0) {
                Toast.makeText(DataEntrySection.this, "Select Date of Interview", Toast.LENGTH_SHORT).show();
            } else if (respondent_name.getText().toString().length() == 0) {
                Toast.makeText(getApplicationContext(), "Enter Respondent Name", Toast.LENGTH_SHORT).show();
                respondent_name.setError("Enter Respondent Name");
            } else if (respondent_tel.getText().toString().length() == 0) {
                Toast.makeText(getApplicationContext(), "Enter Respondent telephone", Toast.LENGTH_SHORT).show();
                respondent_tel.setError("Enter Respondent telephone");
            } else if (rId == -1) {
                Toast.makeText(DataEntrySection.this, "Select Email Availability", Toast.LENGTH_SHORT).show();
            } else if (email.getText().toString().equals("Yes") && availableEmail.getText().toString().isEmpty()) {
                Toast.makeText(DataEntrySection.this, "Please enter Email Id", Toast.LENGTH_SHORT).show();
            } else if (availableEmail.getVisibility() == (View.VISIBLE) && (!DataEntrySection.this.validEmail(val))) {
                Toast.makeText(DataEntrySection.this, "Enter valid EmailId", Toast.LENGTH_SHORT).show();
            } else if (linearLayout16.getVisibility() == (View.VISIBLE) && urn.getText().toString().isEmpty()) {
                Toast.makeText(DataEntrySection.this, "Enter URN", Toast.LENGTH_SHORT).show();
            } else if (QCspinner.getSelectedItem().toString().equals("Select QC")) {
                Toast.makeText(DataEntrySection.this, "Select Type of QC", Toast.LENGTH_SHORT).show();
            } else if (linearLayout19.getVisibility() == (View.VISIBLE) && (noneQC.equals("Accompaniment") || noneQC.equals("Spot Visit")) && (!chk_superviser.isChecked() && !chk_auditor.isChecked() && !chk_eic.isChecked() && !chk_manager.isChecked())) {
                Toast.makeText(DataEntrySection.this, "Select By Whom Field", Toast.LENGTH_SHORT).show();
            } else if (pincode.getText().toString().length() == 0 || pincode.getText().toString().length() < 6) {
                Toast.makeText(DataEntrySection.this, "Enter Correct Pin code", Toast.LENGTH_SHORT).show();
            } else {

                DataEntrySection.this.submitDataEntry();
            }
        });
    }//End of OnCreate


    //Email validation...
    private boolean validEmail(String val) {
        Pattern pattern = Patterns.EMAIL_ADDRESS;
        return pattern.matcher(val).matches();
    }

    private void submitDataEntry() {
        int radioId = email_availability.getCheckedRadioButtonId();
        email = findViewById(radioId);

        //DateOfInterview
        String doa = date_of_interview.getText().toString();
        String new_doa = null;
        try {
            @SuppressLint("SimpleDateFormat") SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
            @SuppressLint("SimpleDateFormat") SimpleDateFormat sdf2 = new SimpleDateFormat("dd/MM/yyyy");
            new_doa = (sdf2.format(sdf.parse(doa)));
            doi = new_doa;
        } catch (ParseException e) {
            e.printStackTrace();
        }


        //TypeOfQC
        int typeqc = 0;
        switch (typeQC) {
            case "Accompaniment":
                typeqc = 1;
                break;
            case "Spot Visit":
                typeqc = 2;
                break;
            case "None":
                typeqc = 3;
                break;
        }

        //ValueOfQC
        int ch1 = 0;
        int ch2 = 0;
        int ch3 = 0;
        int ch4 = 0;

        if (chk_superviser.isChecked()) {
            ch1 = 1;
        }
        if (chk_auditor.isChecked()) {
            ch2 = 1;
        }
        if (chk_eic.isChecked()) {
            ch3 = 1;
        }
        if (chk_manager.isChecked()) {
            ch4 = 1;
        }
        /**

         if (otp_matched.equals("")) {
         AlertDialog.Builder builder = new AlertDialog.Builder(DataEntrySection.this);
         builder.setMessage("Status Not Updated")
         .setCancelable(false)
         .setPositiveButton("OK", (dialog, id) -> dialog.dismiss());
         AlertDialog alert = builder.create();
         alert.show();

         } else {
         */

        // send data to the server
        final HashMap<String, String> params = new HashMap<>();
        params.put("jobname", job_name.getText().toString());
        params.put("jobno", job_no.getText().toString());
        params.put("intid", IntId);
        params.put("intname", int_name.getText().toString());
        params.put("doi", new_doa);
        params.put("fieldoff", field_office.getText().toString());
        params.put("fieldcen", field_center.getText().toString());
        params.put("respname", respondent_name.getText().toString());
        params.put("resptel", respondent_tel.getText().toString());
        params.put("segment", SName);
        params.put("typeofQc", String.valueOf(typeqc));
        params.put("acsup", String.valueOf(ch1));
        params.put("aceic", String.valueOf(ch3));
        params.put("acaud", String.valueOf(ch2));
        params.put("acman", String.valueOf(ch4));
        params.put("empid", uName);
        params.put("nsupid", SupId);
        params.put("nsupname", sup_name.getText().toString().equals("No Name") ? "NULL" : sup_name.getText().toString());
        params.put("neicid", EicId);
        params.put("neicname", eic_name.getText().toString());
        params.put("shopid", "");
        params.put("Pincode", pincode.getText().toString());
        params.put("Urn", urn.getText().toString());
        params.put("UrnStatus", otp_matched);
        params.put("emailavailable", email_check);
        params.put("respprofilesheetdata", String.valueOf(jObjRequestProfileSheetData));
        params.put("teamname", "");
        params.put("EmailID", email.getText().toString().equals("Yes") ? availableEmail.getText().toString().trim() : "");
        Log.e("DataFormat", "format" + params.toString());
        Log.e("DataFormat", "Jsonformat" + new JSONObject(params));
        JsonObjectRequest request9 = new JsonObjectRequest(SaveInterviewData, new JSONObject(params),
                response -> {
                    if (response != null) {
                        progressDialog.dismiss();
                        try {
                            String msg = response.getString("Msg");
                            String record_num = response.getString("recno");
                            if (msg.equals("Record saved successfully.")) {
                                AlertDialog.Builder builder = new AlertDialog.Builder(DataEntrySection.this);
                                builder.setMessage("Record saved successfully !! \n Your record is  " + record_num)
                                        .setCancelable(false)
                                        .setPositiveButton("OK", (dialog, id) -> {
                                            dialog.dismiss();
                                            finish();
                                            startActivity(getIntent());
                                        });
                                AlertDialog alert = builder.create();
                                alert.show();
                                progressDialog.dismiss();
                            }
                            Toast.makeText(this, "" + msg, Toast.LENGTH_SHORT).show();
                        } catch (JSONException e) {
                            e.printStackTrace();
                            Toast.makeText(this, "" + e.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        progressDialog.dismiss();
                        Toast.makeText(this, "Record not saved", Toast.LENGTH_SHORT).show();
                    }
                }, error -> {
            progressDialog.dismiss();
            Toast.makeText(this, "" + error.getStackTrace(), Toast.LENGTH_SHORT).show();
        });
        mQueue9.add(request9);
        /*}*/
    }

    private void verifyOTP() {
        urn.addTextChangedListener(new TextWatcher() {

            public void afterTextChanged(Editable s) {
                if (s.length() == 4) {
                    OTPVerifyData(s.toString());
                } else if (s.length() == 0) {
                    return;
                }
            }

            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }
        });
    }

    private void OTPVerifyData(String s) {
        //DateOfInterview
        String doa = date_of_interview.getText().toString();
        String new_doa = null;
        try {
            @SuppressLint("SimpleDateFormat") SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
            //   @SuppressLint("SimpleDateFormat") SimpleDateFormat sdf2 = new SimpleDateFormat("dd/MM/yyyy");//dd/MM/yyyy
            @SuppressLint("SimpleDateFormat") SimpleDateFormat sdf2 = new SimpleDateFormat("yyyy-MM-dd");//dd/MM/yyyy
            new_doa = (sdf2.format(sdf.parse(doa)));
            doi = new_doa;
        } catch (ParseException e) {
            e.printStackTrace();
        }
        String MobileNo = respondent_tel.getText().toString();
        final HashMap<String, String> params = new HashMap<>();
        params.put("MobileNo", MobileNo);
        params.put("OTP", s);
        params.put("doi", "" + doi);
        Log.e("OTPVERIFY", "VALUES>" + new JSONObject(params));
        Log.e("OTPVERIFY", "VALUES>" + doi);
        Log.e("OTPVERIFY", "url>" + OTPVerifyData);
        progressDialog.show();
        progressDialog.setMessage("Loading");
        progressDialog.setCanceledOnTouchOutside(false);
        JsonObjectRequest request8 = new JsonObjectRequest(OTPVerifyData, new JSONObject(params),
                response -> {
                    try {
                        Log.e("OTPVERIFY", "response>" + response);

                        String reg_status = response.getString("status");
                        if (reg_status.equals("OTP mismatched")) {
                            otp_matched = "N";
                            AlertDialog.Builder builder = new AlertDialog.Builder(DataEntrySection.this);
                            builder.setMessage("OTP mismatched.")
                                    .setCancelable(false)
                                    .setPositiveButton("OK", (dialog, id) -> dialog.dismiss());
                            AlertDialog alert = builder.create();
                            alert.show();
                            progressDialog.dismiss();
                        } else if (reg_status.equals("OTP Verified")) {
                            otp_matched = "Y";
                            AlertDialog.Builder builder = new AlertDialog.Builder(DataEntrySection.this);
                            builder.setMessage("OTP Verified")
                                    .setCancelable(false)
                                    .setPositiveButton("OK", (dialog, id) -> dialog.dismiss());
                            AlertDialog alert = builder.create();
                            alert.show();

                            progressDialog.dismiss();

                        } else {

                            otp_matched = "N";

                        }
                    } catch (JSONException e) {

                        e.printStackTrace();

                    }
                }, error -> progressDialog.dismiss());
        mQueue8.add(request8);
    }

    private void setResTel() {

        respondent_tel.addTextChangedListener(new TextWatcher() {

            public void afterTextChanged(Editable s) {
                if (s.length() == 0 || s.equals("88") || s.equals("99")) {
                    linearLayout16.setVisibility(View.GONE);
                } else if (s.length() == 10) {
                    //URN visibility
                    RespondentTel(s.toString());
                }
            }

            public void beforeTextChanged(CharSequence s, int start,
                                          int count, int after) {
                linearLayout16.setVisibility(View.GONE);
            }

            public void onTextChanged(CharSequence s, int start,
                                      int before, int count) {
            }
        });
    }

    private void RespondentTel(String s) {

        final HashMap<String, String> params = new HashMap<>();

        params.put("MobileNo", s);
        progressDialog.show();
        progressDialog.setMessage("Loading");
        progressDialog.setCanceledOnTouchOutside(false);

        JsonObjectRequest request7 = new JsonObjectRequest(MobileNoData, new JSONObject(params),
                response -> {
                    try {
                        String reg_status = response.getString("status");
                        if (reg_status.equals("OTP is not generated")) {
                            AlertDialog.Builder builder = new AlertDialog.Builder(DataEntrySection.this);
                            builder.setMessage("OTP is not generated.")
                                    .setCancelable(false)
                                    .setPositiveButton("OK", (dialog, id) -> dialog.dismiss());
                            AlertDialog alert = builder.create();
                            alert.show();
                            linearLayout16.setVisibility(View.GONE);
                            progressDialog.dismiss();

                        } else {
                            progressDialog.dismiss();
                            linearLayout16.setVisibility(View.VISIBLE);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }, error -> progressDialog.dismiss());
        mQueue7.add(request7);
    }

    private void setDOI() {

        final Calendar cldr = Calendar.getInstance();
        int day = cldr.get(Calendar.DAY_OF_MONTH);
        int month = cldr.get(Calendar.MONTH);
        int year = cldr.get(Calendar.YEAR);
        // date picker dialog
        picker = new DatePickerDialog(DataEntrySection.this,
                (view, year1, monthOfYear, dayOfMonth) -> date_of_interview.setText(dayOfMonth + "-" + (monthOfYear + 1) + "-" + year1), year, month, day);
        picker.getDatePicker().setMaxDate(System.currentTimeMillis()); //Disable Future Dates...
        picker.show();
    }

    private void getTypesQC() {
        QCspinner.setOnItemSelectedListener(DataEntrySection.this);
        TypesQC.add(0, "Select QC");
        TypesQC.add("Accompaniment");
        TypesQC.add("Spot Visit");
        TypesQC.add("None");
        QCAdapter = new ArrayAdapter<>(this,
                R.layout.support_simple_spinner_dropdown_item, TypesQC);
        QCAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        QCspinner.setAdapter(QCAdapter);
    }

    private void getSegment(String jobName, String uName) {
        final HashMap<String, String> params = new HashMap<>();
        params.put("CenterName", uName);
        params.put("JobName", jobName);
        progressDialog.show();
        progressDialog.setMessage("Loading");
        progressDialog.setCanceledOnTouchOutside(false);
        Log.e("getgetSegment", "getSegmentpsrs>>" + params);
        Log.e("getgetSegment", "sURL>>" + sURL);
        JsonObjectRequest request = new JsonObjectRequest(sURL, new JSONObject(params),
                response -> {
                    try {
                        String SegmentName = response.getString("SegmentName");
                        aList = new ArrayList<>(Arrays.asList(SegmentName.split(",")));
                        for (int i = 0; i < aList.size(); i++) {
                            job = (String) aList.get(i);
                            Snames.add(job);
                            progressDialog.dismiss();
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }, error -> progressDialog.dismiss());
        mQueue.add(request);
    }

    private void getIntId(String uName) {
        IntIdspinner.setOnItemSelectedListener(DataEntrySection.this);
        IntIds.add(0, "Select Int Id");
        IntIds.add(uName);
        intIdsAdapter = new ArrayAdapter<>(this,
                R.layout.support_simple_spinner_dropdown_item, IntIds);
        intIdsAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        IntIdspinner.setAdapter(intIdsAdapter);
    }

    private void interviewerName(String intId) {
        final HashMap<String, String> params = new HashMap<>();
        params.put("IntId", intId);
        params.put("JobNo", JobNo);
        progressDialog.show();
        progressDialog.setMessage("Loading");
        progressDialog.setCanceledOnTouchOutside(false);
        JsonObjectRequest request1 = new JsonObjectRequest(IntName, new JSONObject(params),
                response -> {
                    progressDialog.dismiss();
                    try {
                        String InterviewerName = response.getString("IntName");

                        if (InterviewerName.equals("null")) {
                            int_name.setText("");
                        } else {
                            int_name.setText(InterviewerName);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }, error -> progressDialog.dismiss());
        mQueue1.add(request1);
    }

    private void getSupId(String jobNo, String fieldOff) {

        final HashMap<String, String> params = new HashMap<>();
        params.put("FieldofficeName", fieldOff);
        params.put("JobNo", jobNo);
        progressDialog.show();
        progressDialog.setMessage("Loading");
        progressDialog.setCanceledOnTouchOutside(false);

        JsonObjectRequest request2 = new JsonObjectRequest(SupID, new JSONObject(params),
                response -> {
                    try {
                        String SegmentName = response.getString("SupId");
                        aList = new ArrayList<>(Arrays.asList(SegmentName.split(",")));
                        for (int i = 0; i < aList.size(); i++) {
                            job = (String) aList.get(i);
                            SupIds.add(job);
                            progressDialog.dismiss();
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }, error -> progressDialog.dismiss());
        mQueue2.add(request2);

    }

    private void supervisorName(String supId) {
        final HashMap<String, String> params = new HashMap<>();
        params.put("SupId", supId);
        params.put("JobNo", JobNo);
        progressDialog.show();
        progressDialog.setMessage("Loading");
        progressDialog.setCanceledOnTouchOutside(false);


        JsonObjectRequest request3 = new JsonObjectRequest(SupName, new JSONObject(params),
                response -> {
                    progressDialog.dismiss();
                    try {
                        String InterviewerName = response.getString("SupName");

                        if (InterviewerName.equals("null")) {
                            sup_name.setText("");
                        } else {
                            sup_name.setText(InterviewerName);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }, error -> progressDialog.dismiss());
        mQueue3.add(request3);
    }

    private void getEicId(String jobNo, String fieldOff) {
        final HashMap<String, String> params = new HashMap<>();
        params.put("FieldofficeName", fieldOff);
        params.put("JobNo", jobNo);
        progressDialog.show();
        progressDialog.setMessage("Loading");
        progressDialog.setCanceledOnTouchOutside(false);

        JsonObjectRequest request4 = new JsonObjectRequest(EicID, new JSONObject(params),
                response -> {
                    try {
                        String SegmentName = response.getString("EicId");
                        aList = new ArrayList<>(Arrays.asList(SegmentName.split(",")));
                        for (int i = 0; i < aList.size(); i++) {
                            job = (String) aList.get(i);
                            EicIds.add(job);
                            progressDialog.dismiss();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }, error -> progressDialog.dismiss());
        mQueue4.add(request4);
    }

    private void eicName(String eicId) {
        final HashMap<String, String> params = new HashMap<>();
        params.put("EicId", eicId);
        params.put("JobNo", JobNo);
        progressDialog.show();
        progressDialog.setMessage("Loading");
        progressDialog.setCanceledOnTouchOutside(false);

        JsonObjectRequest request5 = new JsonObjectRequest(EicName, new JSONObject(params),
                response -> {
                    progressDialog.dismiss();
                    try {
                        String InterviewerName = response.getString("EicName");

                        if (InterviewerName.equals("null")) {
                            eic_name.setText("");
                        } else {
                            eic_name.setText(InterviewerName);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }, error -> progressDialog.dismiss());
        mQueue5.add(request5);
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        if (parent.getId() == R.id.segment_spinner) {
            if (parent.getItemAtPosition(position).equals("Select Segment")) {
                return;
            } else {
                SName = parent.getItemAtPosition(position).toString();
            }
        } else if (parent.getId() == R.id.intId_spinner) {
            if (parent.getItemAtPosition(position).equals("Select Int Id")) {
                return;
            } else {
                IntId = parent.getItemAtPosition(position).toString();
                interviewerName(IntId);
            }
        } else if (parent.getId() == R.id.supId_spinner) {
            if (parent.getItemAtPosition(position).equals("Select Sup Id")) {
                return;
            } else if (parent.getItemAtPosition(position).equals("0")) {
                SupId = parent.getItemAtPosition(position).toString();
                sup_name.setText("No Name");
            } else {
                SupId = parent.getItemAtPosition(position).toString();
                supervisorName(SupId);
            }
        } else if (parent.getId() == R.id.eicIdspinner) {
            if (parent.getItemAtPosition(position).equals("Select Eic Id")) {
                return;
            } else {
                EicId = parent.getItemAtPosition(position).toString();
                eicName(EicId);
            }
        } else if (parent.getId() == R.id.types_of_qc) {
            if (parent.getItemAtPosition(position).equals("Select QC")) {
                linearLayout19.setVisibility(View.GONE);
            } else if (parent.getItemAtPosition(position).equals("None")) {
                typeQC = parent.getItemAtPosition(position).toString();
                linearLayout19.setVisibility(View.GONE);
            } else {
                typeQC = parent.getItemAtPosition(position).toString();
                linearLayout19.setVisibility(View.VISIBLE);
            }
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {
    }


    public boolean validate() {
        boolean b = true;


        return b;
    }


}
