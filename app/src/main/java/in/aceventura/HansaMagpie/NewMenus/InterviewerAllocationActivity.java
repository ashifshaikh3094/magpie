package in.aceventura.HansaMagpie.NewMenus;

import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import in.aceventura.HansaMagpie.Config;
import in.aceventura.HansaMagpie.R;
import in.aceventura.HansaMagpie.SharedClass;

import static in.aceventura.HansaMagpie.R.*;

public class InterviewerAllocationActivity extends AppCompatActivity implements AdapterView.OnItemSelectedListener{

    Spinner select_job_name;
    List<String> jobNames = new ArrayList<>();
    ArrayAdapter<String> jobAdapter;
    RequestQueue mQueue;
    ProgressDialog progressDialog;
    String JobName,pURL,job,uName;
    ArrayList aList;
    private Button btnNext;
    SharedClass sharedClass;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(layout.activity_interviewer_allocation);
        select_job_name = findViewById(id.selectJobName);
        btnNext = findViewById(id.btnNext);
        mQueue = Volley.newRequestQueue(this);
        progressDialog = new ProgressDialog(this);
        pURL = Config.PROJECT_URL + "JobName";

        uName = SharedClass.getInstance(this).loadSharedPreference_UserId();
        final HashMap<String, String> params = new HashMap<>();
        params.put("uname", uName);
        params.put("type", "Payment");
        progressDialog.show();
        progressDialog.setMessage("Loading");
        progressDialog.setCanceledOnTouchOutside(false);


        //Api for getting job names
        JsonObjectRequest request = new JsonObjectRequest(pURL, new JSONObject(params), response -> {
            try {
                JobName = response.getString("JobName");
                // Toast.makeText(ActivityPayment.this, ""+JobName, Toast.LENGTH_SHORT).show();
                aList = new ArrayList(Arrays.asList(JobName.split(",")));
                for (int i = 0; i < aList.size(); i++) {
                    job = (String) aList.get(i);
                    if (!JobName.equals("null")) {
                        jobNames.add(job);
                        progressDialog.dismiss();
                    } else {
                        progressDialog.dismiss();
                        return;
                    }
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }

        }, error -> {
            Toast.makeText(this, "Error while fetching job names", Toast.LENGTH_SHORT).show();
        });
        mQueue.add(request);

        select_job_name.setOnItemSelectedListener(InterviewerAllocationActivity.this);
        jobNames.add(0, "Select JobName");
        jobAdapter = new ArrayAdapter<>(this,
                android.R.layout.simple_spinner_dropdown_item, jobNames);
        jobAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        select_job_name.setAdapter(jobAdapter);

        btnNext.setOnClickListener(v -> {

            //call api to get the db & also send the jobname or save it to use on next screen
            String job_name = sharedClass.getjobName(); //getting job_name date from shared pref
        });

    }//On create ends

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int i, long id) {
        if (parent.getId() == R.id.selectJobName) {

            if (parent.getItemAtPosition(i).equals("Select JobName")) {
                Toast.makeText(this, "Select Job Name", Toast.LENGTH_SHORT).show();
            } else {
                String job_name = parent.getItemAtPosition(i).toString();
                //send this job name to the api on click of next button
                //saving job_name in prefs
                SharedClass.jobName(job_name);
            }
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }
}