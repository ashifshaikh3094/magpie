package in.aceventura.HansaMagpie.NewMenus;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

import in.aceventura.HansaMagpie.Config;
import in.aceventura.HansaMagpie.R;
import in.aceventura.HansaMagpie.SharedClass;
import in.aceventura.HansaMagpie.models.JobNameforDataEntryModel;

public class InterviewAllocationActivity extends AppCompatActivity {
    Spinner sp_interview;
    public String pURL = Config.PROJECT_URL;
    Button btnGetDatabase, btnSubmit;
    ArrayList<String> arrayList;
    Context mContext;
    String loginUserID;
    String[] add;
    String value;
    ArrayList<String> same;

    private String valueId = "";
    private String JobName = "";

    String address, district, entryDateTime, fieldCenter, fieldOffice, interviewerId, jobName, recordingTime, recordingType, remarks, respondentName, respondentPhoneNo, segment, state, usedStatus, id;
    private String uname = "";
    JobNameforDataEntryModel model;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_interview_allocation);
        mContext = this;
        init();
        GetDatabase();
        initValue();
    }

    private void init() {
        same = new ArrayList<>();

        arrayList = new ArrayList<>();
        sp_interview = findViewById(R.id.sp_interview);

        btnGetDatabase = findViewById(R.id.btnGetDatabase);

    }

    private void getDetails(String uname, String JobName) {
        ;
        Log.e("INT", "getDetaislvaues" + valueId);
        RequestQueue requestQueue = Volley.newRequestQueue(getBaseContext());
        String url = pURL + "InterviewAllocationData";
        final HashMap<String, String> params = new HashMap<>();
        Log.e("ModelValue", "Val=" + "JOb=" + JobName + "=Name=" + uname);
        params.put("JobName", JobName);
        params.put("InterviewerId", uname);
        params.put("FieldOffice", SharedClass.getInstance(mContext).getFieldOffice());
        Log.e("interview", "AllocationParamiter>>" + params);
        JsonObjectRequest request = new JsonObjectRequest(url, new JSONObject(params), new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.e("InAllo", "Respo===>>" + response);
                try {
                    address = response.getString("Address");
                    district = response.getString("District");
                    entryDateTime = response.getString("EntryDateTime");
                    fieldCenter = response.getString("FieldCenter");
                    fieldOffice = response.getString("FieldOffice");
                    interviewerId = response.getString("InterviewerId");
                    jobName = response.getString("JobName");
                    recordingTime = response.getString("RecordingTime");
                    recordingType = response.getString("RecordingType");
                    remarks = response.getString("Remarks");
                    respondentName = response.getString("RespondentName");
                    respondentPhoneNo = response.getString("RespondentPhoneNo");
                    segment = response.getString("Segment");
                    state = response.getString("State");
                    usedStatus = response.getString("UsedStatus");
                    id = response.getString("id");
                    model = new JobNameforDataEntryModel(address, district, entryDateTime, fieldCenter, fieldOffice, interviewerId, jobName, recordingTime, recordingType, remarks, respondentName, respondentPhoneNo, segment, state, usedStatus, id);
                    if (!(respondentPhoneNo.equals("") || respondentPhoneNo == null || respondentPhoneNo.equals("null"))) {
                        Intent intent = new Intent(mContext, InterviewAllocationJobNameforDataEntry.class);
                        intent.putExtra("JobNameforDataEntryModel", model);
                        startActivity(intent);
                        finish();
                    } else {

                        Toast.makeText(mContext, "No Data for Load...", Toast.LENGTH_SHORT).show();
                    }
                    // Toast.makeText(getApplicationContext(), "" + response, Toast.LENGTH_LONG).show();
                } catch (Exception r) {
                    r.getStackTrace();
                    Log.e("error", "InterviewAllocationData" + r.getMessage());
                }
            }
        }, error -> {
            error.getStackTrace();
            Log.e("msg", "error==>>" + error.getMessage());
        });

        requestQueue.add(request);
    }


    private void initValue() {
        btnGetDatabase.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getDetails(uname, JobName);
                Log.e("", "");

            }
        });

    }

    private void GetDatabase() {


        Log.e("INT", "sha" + SharedClass.getInstance(this).loadSharedPreference_UserId());
        RequestQueue requestQueue = Volley.newRequestQueue(getBaseContext());
        String url = pURL + "JobNameforDataEntry";//JobNameforDataEntry  18-01-2021- ashif //JobName --22/01/2018 > JobNameforDataEntry//22/01/2021
        final HashMap<String, String> params = new HashMap<>();
        params.put("uname", SharedClass.getInstance(this).loadSharedPreference_UserId());
        params.put("type", "Allocation");

        JsonObjectRequest request = new JsonObjectRequest(url, new JSONObject(params), new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.e("InAllo", "Respo===>>" + response);

                try {
                    arrayList.add(response.getString("uname"));
                    Log.e("InAllo", "Respo===>>" + response.getString("uname"));
                    String val = response.getString("JobName");

                    JobName = response.getString("JobName");
                    ArrayList aList = new ArrayList<>(Arrays.asList(JobName.split(",")));
                    for (int i = 0; i < aList.size(); i++) {
                        String job = (String) aList.get(i);
                        if (!JobName.equals("null")) {
                            same.add(job);
                        } else {
                            return;
                        }
                    }
                    SharedClass.jobName(response.getString("JobName"));
                    uname = response.getString("uname");


                } catch (Exception r) {
                    Toast.makeText(getApplicationContext(), r.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }
        }, error -> {
            error.getStackTrace();
            Log.e("msg", "error==>>" + error.getMessage());

        });
        requestQueue.add(request);
        same.add(0, "select");
        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(mContext, android.R.layout.simple_list_item_1, same);
        sp_interview.setAdapter(arrayAdapter);
        arrayAdapter.notifyDataSetChanged();
        sp_interview.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                JobName = parent.getItemAtPosition(position).toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

    }


}