package in.aceventura.HansaMagpie.NewMenus;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;

import in.aceventura.HansaMagpie.R;
import in.aceventura.HansaMagpie.SharedClass;


public class ProfileDetailsActivity extends AppCompatActivity {

    TextView tv_InterviewerID, tv_Name, tv_Address, tv_ContactNumber, tv_Name_In_BankAC, tv_Bank_Name, tv_Branch_Name, tv_AC_No, tv_IFSC_Code;
    String result;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile_details);
        initView();
        initValue();
    }

    private void initView() {

        tv_InterviewerID = findViewById(R.id.tv_InterviewerID);
        tv_Name = findViewById(R.id.tv_Name);
        tv_Address = findViewById(R.id.tv_Address);
        tv_ContactNumber = findViewById(R.id.tv_ContactNumber);
        tv_Name_In_BankAC = findViewById(R.id.tv_Name_In_BankAC);
        tv_Bank_Name = findViewById(R.id.tv_Bank_Name);
        tv_Branch_Name = findViewById(R.id.tv_Branch_Name);
        tv_AC_No = findViewById(R.id.tv_AC_No);
        tv_IFSC_Code = findViewById(R.id.tv_IFSC_Code);

    }

    private void initValue() {
        Log.e("ProfileValue", "Values" + SharedClass.getInstance(this).getIntName());
        tv_InterviewerID.setText(SharedClass.getInstance(this).loadSharedPreference_UserId());
        tv_Name.setText(SharedClass.getInstance(this).getIntName());
        tv_Address.setText(SharedClass.getInstance(this).getAddress());

        try {
            String phone = SharedClass.getInstance(this).getContact_Number();
            //todo 99*****529
            result = phone.substring(0, 2) + "*****" + phone.substring(7, 10);
            Log.e("ProfileValue", "Phone>" + result);

            Log.e("ProfileValue", "Phone>" + phone + result);


        } catch (Exception e) {
            Log.e("ProfileValue", "error00000>" + e.getMessage());

        }
        tv_ContactNumber.setText(result);
        tv_Name_In_BankAC.setText(SharedClass.getInstance(this).getBankAcName());
        tv_Bank_Name.setText(SharedClass.getInstance(this).getBank());
        tv_Branch_Name.setText(SharedClass.getInstance(this).getBranch());
        tv_IFSC_Code.setText(SharedClass.getInstance(this).getIfscCode());
        tv_AC_No.setText(SharedClass.getInstance(this).getAC_No());

    }
}