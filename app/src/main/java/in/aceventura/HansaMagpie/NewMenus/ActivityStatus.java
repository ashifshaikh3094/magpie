package in.aceventura.HansaMagpie.NewMenus;

import android.content.Intent;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;

import android.widget.Button;
import android.widget.Toast;

import in.aceventura.HansaMagpie.ActivityPayment;
import in.aceventura.HansaMagpie.ActivityQCS;
import in.aceventura.HansaMagpie.ActivitySummary;
import in.aceventura.HansaMagpie.InterviewAllocationStatusReport;
import in.aceventura.HansaMagpie.R;

public class ActivityStatus extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_status);

        Button QCS = findViewById(R.id.QCstatus);
        Button summary = findViewById(R.id.summary);
        Button payment = findViewById(R.id.payment);
        Button bt_InterviewAllocationStatusReport = findViewById(R.id.bt_InterviewAllocationStatusReport);

        QCS.setOnClickListener(v -> {
            Intent intent = new Intent(ActivityStatus.this, ActivityQCS.class);
            startActivity(intent);
            Toast.makeText(ActivityStatus.this, "QC status", Toast.LENGTH_SHORT).show();


        });

        summary.setOnClickListener(v -> {
            Intent intent = new Intent(ActivityStatus.this, ActivitySummary.class);
            startActivity(intent);
            Toast.makeText(ActivityStatus.this, "Summary of Interviews", Toast.LENGTH_SHORT).show();
        });

        payment.setOnClickListener(v -> {
            Intent intent = new Intent(ActivityStatus.this, ActivityPayment.class);
            startActivity(intent);
            Toast.makeText(ActivityStatus.this, "Payment Status", Toast.LENGTH_SHORT).show();
        });
        bt_InterviewAllocationStatusReport.setOnClickListener(v -> {
            Intent intent = new Intent(ActivityStatus.this, InterviewAllocationStatusReport.class);
            startActivity(intent);
            Toast.makeText(ActivityStatus.this, "Interview Allocation Status Report", Toast.LENGTH_SHORT).show();
        });
    }
}
