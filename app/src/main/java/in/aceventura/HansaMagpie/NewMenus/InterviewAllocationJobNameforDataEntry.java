package in.aceventura.HansaMagpie.NewMenus;

import androidx.appcompat.app.AppCompatActivity;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.Intent;

import android.net.Uri;
import android.os.Bundle;

import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;


import org.json.JSONObject;

import java.io.File;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;

import in.aceventura.HansaMagpie.Config;
import in.aceventura.HansaMagpie.R;
import in.aceventura.HansaMagpie.SharedClass;
import in.aceventura.HansaMagpie.models.JobNameforDataEntryModel;

public class InterviewAllocationJobNameforDataEntry extends AppCompatActivity {
    public final static String TAG = "JobNameforDataEntry";
    JobNameforDataEntryModel model;
    EditText et_Name, et_State, et_District, et_FieldCenter, et_Address, et_Segment, et_Remark, et_PhoneNo;
    public String pURL = Config.PROJECT_URL;
    String[] callStatus = {"select", "Successful", "Not Eligible", "Call Later", "Wrong Number", "Ringing", "Switch Off", "Engaged", "Not Reachable", "Number out of service", "Person not available", "Refused", "Language Problem"};
    Spinner sp_callingStatus;
    Button btnSubmit, btnCall;
    Context mContext;
    String CallStatus;
    TextView tv_date, tv_time;
    DatePickerDialog picker;
    String callLetterDateTime = "";
    String Date;
    String Time;
    LinearLayout ll_dateTime;
    //  CallRecord callRecord;
    // String encoded;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_interview_allocation_job_namefor_data_entry);
        try {
            mContext = this;
            Intent intent = getIntent();
            model = (JobNameforDataEntryModel) intent.getExtras().getSerializable("JobNameforDataEntryModel");
            Log.e(TAG, "Value" + model.getRespondentPhoneNo() + "" + model.getId());
        } catch (Exception e) {
            e.getStackTrace();
            Log.e(TAG, "Modelerror" + e.getMessage());
        }
        initView();
        initValue();
    }


    private void initView() {
        et_Name = findViewById(R.id.et_Name);
        btnCall = findViewById(R.id.btnCall);
        tv_date = findViewById(R.id.tv_date);
        tv_time = findViewById(R.id.tv_time);
        et_State = findViewById(R.id.et_State);
        ll_dateTime = findViewById(R.id.ll_dateTime);
        et_District = findViewById(R.id.et_District);
        et_FieldCenter = findViewById(R.id.et_FieldCenter);
        et_Address = findViewById(R.id.et_Address);
        et_Segment = findViewById(R.id.et_Segment);
        et_Remark = findViewById(R.id.et_Remark);
        et_PhoneNo = findViewById(R.id.et_PhoneNo);
        sp_callingStatus = findViewById(R.id.sp_callingStatus);
        btnSubmit = findViewById(R.id.btnSubmit);
        et_Address.setText(model.getAddress());
        et_District.setText(model.getDistrict());
        et_FieldCenter.setText(model.getFieldCenter());
        et_Remark.setText(model.getRemarks());
        et_Name.setText(model.getRespondentName());
        et_PhoneNo.setText(model.getRespondentPhoneNo());
        et_Segment.setText(model.getSegment());
        et_State.setText(model.getState());
        et_Name.setEnabled(false);
        et_State.setEnabled(false);
        et_District.setEnabled(false);
        et_FieldCenter.setEnabled(false);
        et_Address.setEnabled(false);
        et_Segment.setEnabled(false);
        et_PhoneNo.setEnabled(false);
        et_Remark.setEnabled(false);

    }

    private void initValue() {

        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<>(mContext, android.R.layout.simple_list_item_1, callStatus);
        sp_callingStatus.setAdapter(arrayAdapter);
        sp_callingStatus.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                CallStatus = parent.getItemAtPosition(position).toString();
                if (CallStatus.equals("Call Later")) {
                    ll_dateTime.setVisibility(View.VISIBLE);
                    tv_date.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            final Calendar cldr = Calendar.getInstance();
                            int day = cldr.get(Calendar.DAY_OF_MONTH);
                            int month = cldr.get(Calendar.MONTH);
                            int year = cldr.get(Calendar.YEAR);
                            // date picker dialog
                            picker = new DatePickerDialog(
                                    mContext,
                                    (view, year1, monthOfYear, dayOfMonth) -> {
                                        tv_date.setText(dayOfMonth + "-" + (monthOfYear + 1) + "-" + year1);
                                        //callLetterDateTime = callLetterDateTime + dayOfMonth + "/" + (monthOfYear + 1) + "/" + year1 + " ";//dd/mm/yy
                                        callLetterDateTime = callLetterDateTime + (monthOfYear + 1) + "/" + dayOfMonth + "/" + year1 + " ";//mm/dd/yy

                                        Log.e("DateTME", "Date=" + Date);
                                    }, year, month, day);
                            picker.getDatePicker().setMinDate(System.currentTimeMillis());//Disable Future Dates...
                            picker.show();
                        }
                    });
                    tv_time.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            // TODO Auto-generated method stub
                            Calendar mcurrentTime = Calendar.getInstance();
                            int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
                            int minute = mcurrentTime.get(Calendar.MINUTE);
                            TimePickerDialog mTimePicker;
                            mTimePicker = new TimePickerDialog(mContext, new TimePickerDialog.OnTimeSetListener() {
                                @Override
                                public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
                                    Time = selectedHour + ":" + selectedMinute;
                                    Log.e("DateTME", "TIME=" + Time);
                                    SimpleDateFormat fmt = new SimpleDateFormat("HH:mm");
                                    Date date = null;
                                    try {
                                        date = fmt.parse(Time);
                                    } catch (ParseException e) {
                                        e.printStackTrace();
                                    }
                                    SimpleDateFormat fmtOut = new SimpleDateFormat("hh:mm aa");
                                    String formattedTime = fmtOut.format(date).toString().trim();
                                    tv_time.setText(formattedTime);
                                    callLetterDateTime = callLetterDateTime + formattedTime;
                                }
                            }, hour, minute, false);//Yes 24 hour time
                            mTimePicker.setTitle("Select Time");
                            mcurrentTime.setTimeZone(Calendar.getInstance().getTimeZone());
                            mTimePicker.show();
                        }
                    });
                    Log.e("CallLaterDatetime", "CallLaterDatetime" + callLetterDateTime);
                } else {
                    callLetterDateTime = "";
                    ll_dateTime.setVisibility(View.GONE);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });
        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    // callRecord.stopCallReceiver();
                    //  Toast.makeText(getApplicationContext(), "buttonSubmit=?" + Environment.getExternalStorageDirectory().getPath() + "/MAGPIE", Toast.LENGTH_SHORT).show();
                    // File mAudioFile = getLastModifiedFile(Environment.getExternalStorageDirectory().getAbsolutePath().toString() + "/MAGPIE");
                    // byte[] bytes = FileUtils.readFileToByteArray(mAudioFile);
                    // encoded = Base64.encodeToString(bytes, 0);

                    // Toast.makeText(getApplicationContext(), "Fils=>Name" + callRecord.getRecordFileName(), Toast.LENGTH_SHORT).show();
                    // Toast.makeText(getApplicationContext(), "clcli" + encoded, Toast.LENGTH_SHORT).show();
                } catch (Exception e) {
                    e.getStackTrace();
                    //Log.e("", "" + callLetterDateTime);
                }
                Log.e("CalS", "Cal S==??" + CallStatus);
                Log.e("CalS", "time==??" + tv_time.getText());
                Log.e("CalS", "Date==??" + tv_date.getText());
                if (CallStatus.equals("Call Later")) {
                    if (tv_time.getText().toString().trim().length() == 0 || tv_time.getText().toString().equals("") || tv_date.getText().toString().trim().length() == 0 || tv_date.getText().toString().equals("")) {
                        Toast.makeText(getApplicationContext(), "Please Choose  Date and Time Call Letter", Toast.LENGTH_SHORT).show();
                        Log.e("CalS", "Calinside S==??" + CallStatus);
                    } else {
                        Log.e("REC", "RECTYPE==??" + model.getRecordingType());
                        getInterviewAllocationStatus(model.getAddress(), model.getDistrict(), model.getEntryDateTime(), model.getFieldCenter(), model.getFieldOffice(), model.getInterviewerId(), model.getJobName(), model.getRecordingTime(), model.getRecordingType(), model.getRemarks(), model.getRespondentName(), model.getRespondentPhoneNo(), model.getSegment(), model.getState(), model.getUsedStatus(), model.getId());
                    }
                } else {
                    //
                    Log.e("outside", "Calinside else S==??" + CallStatus);
                    // getInterviewAllocationStatus(model.getAddress(), model.getDistrict(), model.getEntryDateTime(), model.getFieldCenter(), model.getFieldOffice(), model.getInterviewerId(), model.getJobName(), model.getRecordingTime(), model.getRecordingType(), model.getRemarks(), model.getRespondentName(), model.getRespondentPhoneNo(), model.getSegment(), model.getState(), model.getUsedStatus(), model.getId());
                    if (CallStatus.equals("select")) {
                        Toast.makeText(getApplicationContext(), "Please select Call Status", Toast.LENGTH_SHORT).show();
                    } else {
                        //Toast.makeText(getApplicationContext(), "submit Clike -=>" + encoded, Toast.LENGTH_SHORT).show();
                        getInterviewAllocationStatus(model.getAddress(), model.getDistrict(), model.getEntryDateTime(), model.getFieldCenter(), model.getFieldOffice(), model.getInterviewerId(), model.getJobName(), model.getRecordingTime(), model.getRecordingType(), model.getRemarks(), model.getRespondentName(), model.getRespondentPhoneNo(), model.getSegment(), model.getState(), model.getUsedStatus(), model.getId());
                    }
                }
            }
        });
        btnCall.setOnClickListener((v) -> {
            try {
                try {
                    /*Toast.makeText(InterviewAllocationJobNameforDataEntry.this, "Call Record ", Toast.LENGTH_LONG).show();
                    callRecord = new CallRecord.Builder(InterviewAllocationJobNameforDataEntry.this)
                            .setLogEnable(true)
                            .setRecordDirName(Environment.getExternalStorageDirectory() + "/MAGPIE")// optional & default value
                            .setAudioSource(MediaRecorder.AudioSource.VOICE_COMMUNICATION) // optional & default value
                            .setShowSeed(true) // optional & default value ->Ex: RecordFileName_incoming.amr || RecordFileName_outgoing.amr
                            .build();
                    callRecord.startCallReceiver();*/
                } catch (Exception e) {
                    e.getStackTrace();
                    // Toast.makeText(mContext, "Permition Not Given By User", Toast.LENGTH_SHORT).show();
                }

                String number = model.getRespondentPhoneNo();
                Intent intent = new Intent(Intent.ACTION_DIAL);
                intent.setData(Uri.parse("tel:" + number));
                startActivity(intent);
            } catch (Exception e) {
                e.getStackTrace();
                Log.e("INT", "CallDIAL" + e.getStackTrace());
            }
            //  Toast.makeText(getApplicationContext(), "" + callRecord.getRecordDirPath().toString(), Toast.LENGTH_SHORT).show();
        });
    }

    private File getLastModifiedFile(String toString) {
        File directory = new File(toString);
        File[] files = directory.listFiles(File::isFile);
        long lastModifiedTime = Long.MIN_VALUE;
        File chosenFile = null;
        if (files != null) {
            for (File file : files) {
                if (file.lastModified() > lastModifiedTime) {
                    chosenFile = file;
                    lastModifiedTime = file.lastModified();
                }
            }
        }
        return chosenFile;
    }

    public void getInterviewAllocationStatus(String address, String district, String entryDateTime, String fieldCenter, String fieldOffice, String interviewerId, String jobName, String recordingTime, String recordingType, String remarks, String respondentName, String respondentPhoneNo, String segment, String state, String usedStatus, String id) {
        //adding call stestus Part Full or Not
        // Toast.makeText(getApplicationContext(), "Submit File Name=" + callRecord.getRecordFileName(), Toast.LENGTH_SHORT).show();
        RequestQueue requestQueue = Volley.newRequestQueue(getBaseContext());
        Log.e("Submit", "ValueCallingS" + CallStatus);
        Log.e("Submit", "Address" + address);
        Log.e("CallLaterDatetime", "CallLaterDatetime" + callLetterDateTime);
        String url = pURL + "InterviewAllocationStatus";
        Log.e("InterAllocation", "msgparam" + url);
        final HashMap<String, String> params = new HashMap<>();
        params.put("Address", address);
        params.put("District", district);
        params.put("EntryDateTime", entryDateTime);
        params.put("FieldCenter", fieldCenter);
        params.put("FieldOffice", fieldOffice);
        Log.e("INTFIle", "Valueis=" + SharedClass.getInstance(this).loadSharedPreference_UserId());
        params.put("InterviewerId", SharedClass.getInstance(this).loadSharedPreference_UserId());//userId
        params.put("JobName", jobName);
        params.put("RecordingTime", recordingTime);
        params.put("RecordingType", recordingType);
        params.put("Remarks", remarks);
        params.put("RespondentName", respondentName);
        params.put("RespondentPhoneNo", respondentPhoneNo);
        params.put("Segment", segment);
        params.put("State", state);
        params.put("CallStatus", CallStatus);
        params.put("id", id);
        params.put("RecordingFile", "");

        params.put("RecordingFiledata", "");
        params.put("CallLaterDatetime", callLetterDateTime);
        Log.e("InterAllocation", "msgparam" + params);
        //  Toast.makeText(getApplicationContext(), "Submit Time Convert=" + encoded, Toast.LENGTH_SHORT).show();

        JsonObjectRequest request = new JsonObjectRequest(url, new JSONObject(params), new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.e("InterAllocation", "msgresponse" + response);
                Log.e(TAG, "Respo=JobNameforDataEntry==>>" + response);
                /*try {
                    if (!(respondentPhoneNo.equals("") || respondentPhoneNo == null || respondentPhoneNo.equals("null"))) {
                    } else {
                        RequestQueue requestQueue = Volley.newRequestQueue(getBaseContext());
                        String url = pURL + "InterviewAllocationData";
                        final HashMap<String, String> params = new HashMap<>();
                        params.put("JobName", "");
                        params.put("InterviewerId", "");
                        JsonObjectRequest request = new JsonObjectRequest(url, new JSONObject(params), new Response.Listener<JSONObject>() {
                            @Override
                            public void onResponse(JSONObject response) {
                            }
                        }, new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                            }
                        });
                        requestQueue.add(request);
                    }
                    Toast.makeText(mContext, "" + response, Toast.LENGTH_SHORT).show();
                } catch (Exception r) {
                    r.getMessage();
                    r.printStackTrace();
                    Log.e("", "" + r.getMessage());
                }*/
                startActivity(new Intent(mContext, InterviewAllocationActivity.class));
                finish();
                Toast.makeText(mContext, "Record saved Successful", Toast.LENGTH_SHORT).show();
            }
        }, error -> {
            error.getStackTrace();
            Log.e("InterAllocation", "msgresponse" + error.getMessage() + "Strace=>" + error.getStackTrace());
            error.getStackTrace();
            error.printStackTrace();

            Log.e("msg", "error==>>" + error.getMessage());
        });
        requestQueue.add(request);
    }
}
