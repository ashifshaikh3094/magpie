package in.aceventura.HansaMagpie;

import java.io.Serializable;

/**
 * Created by abhinav on 5/2/18.
 */

public class AddStudentBean implements Serializable {
    private String id,StudName,Mobile_No,RollNo,Int_Id,isSync,OTP;

    public String getIsSync() {
        return isSync;
    }

    public void setIsSync(String isSync) {
        this.isSync = isSync;
    }

    public String getMobile_No() {
        return Mobile_No;
    }

    public void setMobile_No(String mobile_No) {
        Mobile_No = mobile_No;
    }

    public String getInt_Id() {
        return Int_Id;
    }

    public void setInt_Id(String int_Id) {
        Int_Id = int_Id;
    }

    public String getOTP() {
        return OTP;
    }

    public void setOTP(String OTP) {
        this.OTP = OTP;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getStudName() {
        return StudName;
    }

    public void setStudName(String studName) {
        StudName = studName;
    }



    public String getRollNo() {
        return RollNo;
    }

    public void setRollNo(String rollNo) {
        RollNo = rollNo;
    }



}
