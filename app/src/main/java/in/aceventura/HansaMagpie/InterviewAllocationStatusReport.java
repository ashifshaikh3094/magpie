package in.aceventura.HansaMagpie;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

import in.aceventura.HansaMagpie.adapters.RecordsAdapter;
import in.aceventura.HansaMagpie.adapters.StatusReportAdapter;
import in.aceventura.HansaMagpie.models.DataModel;
import in.aceventura.HansaMagpie.models.StatusReportModel;

public class InterviewAllocationStatusReport extends AppCompatActivity {
    Spinner sp_JobName;
    RequestQueue requestQueue;
    Context mContext;
    public String pURL = Config.PROJECT_URL;

    TextView tv_Records;
    String recordValues;
    private String jobname = "";
    private String spjobname = "";
    RecyclerView rv_InterviewAllocationStatusReport;
    List<DataModel> statusReportModels;
    RecordsAdapter statusReportAdapter;
    ArrayList<DataModel> dataModelList;
    ArrayList<String> same;
    ArrayList<String> same1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_interview_allocation_status_report);
        mContext = this;
        same = new ArrayList<>();
        same1 = new ArrayList<>();
        initView();
        GetDatabase();
        // initView();

        // interviewAllocationStatusReport();
        initValue();


    }


    private void initView() {
        requestQueue = Volley.newRequestQueue(getBaseContext());
        sp_JobName = findViewById(R.id.sp_JobName);
        tv_Records = findViewById(R.id.tv_Records);
        rv_InterviewAllocationStatusReport = findViewById(R.id.rv_InterviewAllocationStatusReport);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        rv_InterviewAllocationStatusReport.setLayoutManager(layoutManager);
        rv_InterviewAllocationStatusReport.setItemAnimator(new DefaultItemAnimator());
        statusReportModels = new ArrayList<>();
        dataModelList = new ArrayList<>();

    }

    private void initValue() {

        tv_Records.setText("");
    }

    private void interviewAllocationStatusReport(String value) {
        Log.e("llvalue", "na" + value);
        //Api for getting job names

        String url = pURL + "InterviewAllocationStatusReport";
        final HashMap<String, String> params = new HashMap<>();
        Log.e("StatusReport", "JobNAme" + SharedClass.getInstance(this).getjobName());
        Log.e("StatusReport", "Uname" + SharedClass.getInstance(this).loadSharedPreference_UserId());


        params.put("JobName", value);//SharedClass.getInstance(this).getjobName()
        params.put("uname", SharedClass.getInstance(this).loadSharedPreference_UserId());
        Log.e("params", "param" + params);

        JsonObjectRequest request = new JsonObjectRequest(url, new JSONObject(params), response -> {
            try {

                same.add(0, "select");
                jobname = response.getString("JobName");
                ArrayList aList = new ArrayList<>(Arrays.asList(jobname.split(",")));
                for (int i = 0; i < aList.size(); i++) {
                    String job = (String) aList.get(i);
                    if (!jobname.equals("null")) {
                        same.add(job);
                    } else {
                        return;
                    }
                }


                Log.e("Report", "Stuts" + response);
                jobname = response.getString("JobName");
                if (jobname.equals("null")) {
                    Log.e("Sreport", "nullvalues" + jobname);

                } else {
                    Log.e("Sreport", "notnull" + jobname);
                }

                //  ArrayList aList = new ArrayList<>(Arrays.asList(jobname.split(",")));

                response.getString("uname");

                recordValues = response.getString("Records");

                recordValues = recordValues.replace("[{", "");
                recordValues = recordValues.replace("}]", "");
                Log.e("ValuesDa", "D" + recordValues);

                Log.e("ValuesDa", "JobbName" + jobname);
                try {
                    String[] splitStr1 = recordValues.split(",");
                    Log.e("vaRepos", "" + recordValues);
                    for (String s : splitStr1) {
                        String[] splitStr2 = s.split(":");
                        dataModelList.add(new DataModel(splitStr2[0], splitStr2[1]));
                        Log.e("ValuesDa", "S" + splitStr2[0] + "sVs" + splitStr2[1]);
                        statusReportAdapter = new RecordsAdapter(mContext, dataModelList);

                    }
                    rv_InterviewAllocationStatusReport.setAdapter(statusReportAdapter);
                    statusReportAdapter.notifyDataSetChanged();
                } catch (Exception e) {
                    e.printStackTrace();
                }


               /* ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(mContext, android.R.layout.simple_list_item_1, same);
                sp_JobName.setAdapter(arrayAdapter);
                sp_JobName.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                        dataModelList.clear();
                        Log.e("ValuesDa", "JobbName same" + jobname + "-Sa-" + parent.getItemAtPosition(position));
                        if (jobname.toString().trim().equals(parent.getItemAtPosition(position).toString().trim())) {
                            // tv_Records.setText(recordValues);
                            try {
                                String[] splitStr1 = recordValues.split(",");
                                Log.e("vaRepos", "" + recordValues);
                                for (String s : splitStr1) {
                                    String[] splitStr2 = s.split(":");
                                    dataModelList.add(new DataModel(splitStr2[0], splitStr2[1]));
                                    Log.e("ValuesDa", "S" + splitStr2[0] + "sVs" + splitStr2[1]);
                                    statusReportAdapter = new RecordsAdapter(mContext, dataModelList);

                                }
                                rv_InterviewAllocationStatusReport.setAdapter(statusReportAdapter);
                                statusReportAdapter.notifyDataSetChanged();
                            } catch (Exception e) {

                            }
                        } else {
                            try {
                                dataModelList.clear();
                                rv_InterviewAllocationStatusReport.setAdapter(statusReportAdapter);
                                statusReportAdapter.notifyDataSetChanged();

                            } catch (Exception e) {
                                e.getMessage();
                            }

                        }


                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> parent) {

                    }
                });*/

            } catch (JSONException e) {
                e.printStackTrace();
            }

        }, error -> {
            Toast.makeText(this, "Error while fetching job names", Toast.LENGTH_SHORT).show();
        });
        requestQueue.add(request);

    }


    private void GetDatabase() {

        same1.add(0, "Select");
        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(mContext, android.R.layout.simple_list_item_1, same1);
        sp_JobName.setAdapter(arrayAdapter);


        Log.e("INT", "sha" + SharedClass.getInstance(this).loadSharedPreference_UserId());
        RequestQueue requestQueue = Volley.newRequestQueue(getBaseContext());
        String url = pURL + "JobNameforDataEntry";//JobNameforDataEntry(3.6)<Change>  -- 18-01-2021 /JobName(3.7)<New>->22/01/2021/ >JobNameforDataEntry(3.8 again in Report n interAllocation) <New>
        final HashMap<String, String> params = new HashMap<>();
        params.put("uname", SharedClass.getInstance(this).loadSharedPreference_UserId());
        params.put("type", "Allocation");
        JsonObjectRequest request = new JsonObjectRequest(url, new JSONObject(params), new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.e("InAllo", "Respo===>>" + response);

                try {

                    same1.clear();
                    spjobname = response.getString("JobName");
                    ArrayList aList1 = new ArrayList<>(Arrays.asList(spjobname.split(",")));
                    for (int i = 0; i < aList1.size(); i++) {
                        String job = (String) aList1.get(i);
                        if (!spjobname.equals("null")) {
                            same1.add(job);
                        } else {
                            return;
                        }
                    }
                    same1.add(0, "Select");


                    SharedClass.jobName(response.getString("JobName"));


                    ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(mContext, android.R.layout.simple_list_item_1, same1);
                    sp_JobName.setAdapter(arrayAdapter);

                    sp_JobName.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                            parent.getSelectedItem();
                            dataModelList.clear();
                            statusReportAdapter = new RecordsAdapter(mContext, dataModelList);
                            rv_InterviewAllocationStatusReport.setAdapter(statusReportAdapter);
                            statusReportAdapter.notifyDataSetChanged();
                            Log.e("posJob", "valuesSele=" + parent.getSelectedItem());
                            Log.e("posJob", "valuesPos=" + parent.getItemAtPosition(position));
                            interviewAllocationStatusReport(parent.getSelectedItem().toString());
                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> parent) {

                        }
                    });
                    //SharedClass.jobName(response.getString("JobName"));


                } catch (Exception r) {
                }
            }
        }, error -> {
            error.getStackTrace();
            Log.e("msg", "error==>>" + error.getMessage());
        });
        requestQueue.add(request);
    }
}