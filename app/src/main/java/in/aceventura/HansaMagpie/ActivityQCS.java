package in.aceventura.HansaMagpie;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TableLayout;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import in.aceventura.HansaMagpie.adapters.RecordsAdapter;
import in.aceventura.HansaMagpie.models.DataModel;

public class ActivityQCS extends AppCompatActivity implements AdapterView.OnItemSelectedListener {

    Spinner ProjectspinnerQ, CentrespinnerQ;
    TableLayout tableLayout;
    public static String JobName;
    public static String CenterName;
    ProgressDialog progressDialog;

    public String pURL = Config.PROJECT_URL + "JobName";
    public String cURL = Config.PROJECT_URL + "CenterName";
    public String dURL = Config.PROJECT_URL + "QCStatus";


    List<String> Pnames = new ArrayList<>();
    List<String> Cnames = new ArrayList<>();
    ArrayList aList;
    //ArrayList recordList;
    public String projectItem;
    public String CName;

    public String uName;
    public List<DataModel> dataModelList = new ArrayList<>();
    public RequestQueue mQueue1, mQueue2;
    ArrayAdapter<String> cAdapter;

    @BindView(R.id.textView)
    TextView textView;
    @BindView(R.id.textView2)
    TextView textView2;
    @BindView(R.id.rv_records)
    RecyclerView rvRecords;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_qcs);
        textView = findViewById(R.id.textView);
        textView2 = findViewById(R.id.textView2);
        rvRecords = findViewById(R.id.rv_records);
        progressDialog = new ProgressDialog(this);


        LinearLayoutManager layoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        rvRecords.setLayoutManager(layoutManager);
        rvRecords.setItemAnimator(new DefaultItemAnimator());

        ProjectspinnerQ = findViewById(R.id.Pspinner);
        CentrespinnerQ = findViewById(R.id.Cspinner);
        tableLayout = findViewById(R.id.table);
        RequestQueue mQueue = Volley.newRequestQueue(this);
        mQueue1 = Volley.newRequestQueue(this);
        mQueue2 = Volley.newRequestQueue(this);
        uName = SharedClass.getInstance(this).loadSharedPreference_UserId();


        //JobName Api
        final HashMap<String, String> params = new HashMap<>();
        params.put("uname", uName);
        progressDialog.show();
        progressDialog.setMessage("Loading");
        progressDialog.setCanceledOnTouchOutside(false);


        JsonObjectRequest request = new JsonObjectRequest(pURL, new JSONObject(params), response -> {
            try {
                JobName = response.getString("JobName");


                ArrayList aList = new ArrayList<>(Arrays.asList(JobName.split(",")));
                for (int i = 0; i < aList.size(); i++) {
                    String job = (String) aList.get(i);
                    if (!JobName.equals("null")) {
                        Pnames.add(job);
                        progressDialog.dismiss();
                    }
                    else {
                        progressDialog.dismiss();
                        return;
                    }
                }

            }
            catch (JSONException e) {
                e.printStackTrace();
            }

        }, error -> Log.d("JOB_NAME_ERROR", error.toString()));
        mQueue.add(request);
        ProjectspinnerQ.setOnItemSelectedListener(this);
        //Cnames.clear();

        Pnames.add(0, "Select JobName");
        ArrayAdapter<String> pAdapter = new ArrayAdapter<>(this,
                android.R.layout.simple_spinner_dropdown_item, Pnames);
        pAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        pAdapter.notifyDataSetChanged();
        ProjectspinnerQ.setAdapter(pAdapter);


        //------------------------

        CentrespinnerQ.setOnItemSelectedListener(this);

        Cnames.add(0, "Select Center");
        //Cnames.clear();
        cAdapter = new ArrayAdapter<>(this,
                R.layout.support_simple_spinner_dropdown_item, Cnames);
        CentrespinnerQ.setAdapter(cAdapter);

    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        if (parent.getId() == R.id.Pspinner) {
            if (parent.getItemAtPosition(position).equals("Select JobName")) {
                return;

            } else {
                rvRecords.setVisibility(View.GONE);
                dataModelList.clear();
                cAdapter.clear();
                Cnames.add(0, "Select Center");
                dataModelList.clear();
                projectItem = parent.getItemAtPosition(position).toString();
                cAdapter.notifyDataSetChanged();
                getCenter(projectItem);


            }

        } else if (parent.getId() == R.id.Cspinner) {

            if (parent.getItemAtPosition(position).equals("Select Center")) {
                return;
            } else {
                //operations
                CName = parent.getItemAtPosition(position).toString();
                getDetails(CName);


            }
        }
    }

    ///*
    private void getDetails(String CName) {
        final HashMap<String, String> params1 = new HashMap<>();
        params1.put("uname", uName);
        params1.put("JobName", projectItem);
        params1.put("CenterName", CName);
        progressDialog.show();
        progressDialog.setMessage("Loading");
        progressDialog.setCanceledOnTouchOutside(false);
        //to reset the details
        rvRecords.setVisibility(View.VISIBLE);
        dataModelList.clear();

        JsonObjectRequest request2 = new JsonObjectRequest(dURL, new JSONObject(params1),
                response -> {
                    String sampleArrStr;
                    try {
                        sampleArrStr = response.getString("Records");
                        sampleArrStr = sampleArrStr.replace("[{", "");
                        sampleArrStr = sampleArrStr.replace("}]", "");


                        String[] splitStr1 = sampleArrStr.split(",");
                        for (String s : splitStr1) {
                            String[] splitStr2 = s.split(":");
                            dataModelList.add(new DataModel(splitStr2[0], splitStr2[1]));
                        }


                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    rvRecords.setAdapter(new RecordsAdapter(getApplicationContext(), dataModelList));
                    progressDialog.dismiss();


                }, error -> {

        });
        mQueue2.add(request2);
    }

    //*/
    private void getCenter(String projectItem) {
        final HashMap<String, String> params = new HashMap<>();
        params.put("uname", uName);
        params.put("JobName", projectItem);
        progressDialog.show();
        progressDialog.setMessage("Loading");
        progressDialog.setCanceledOnTouchOutside(false);

        JsonObjectRequest request1 = new JsonObjectRequest(cURL, new JSONObject(params),
                response -> {
                    try {
                        CenterName = response.getString("CenterName");
                        aList = new ArrayList(Arrays.asList(CenterName.split(",")));
                        for (int i = 0; i < aList.size(); i++) {
                            String job = (String) aList.get(i);
                            Cnames.add(job);
                            progressDialog.dismiss();
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }, error -> {

        });
        mQueue1.add(request1);
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }
}
