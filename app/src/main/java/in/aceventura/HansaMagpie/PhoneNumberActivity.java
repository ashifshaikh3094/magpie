package in.aceventura.HansaMagpie;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.android.volley.RequestQueue;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class PhoneNumberActivity extends AppCompatActivity {

    public static final String MyPREFERENCES = "MyPrefs";
    public static final String LOGGEDIN_SHARED_PREF = "loggedin";

    SharedPreferences sharedpreferences;

    TextView editTextCountryCode;
    EditText editTextPhone, editTextuserid;
    Button buttonContinue;

    String len;
    String IntName;
    String MSG;
    String fieldOffice;
    String pwd;
    String status;
    String uname;
    String code;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_phone_number);

        editTextCountryCode = findViewById(R.id.editTextCountryCode);
        editTextPhone = findViewById(R.id.editTextPhone);
        editTextuserid = findViewById(R.id.editTextuserid);
        buttonContinue = findViewById(R.id.buttonContinue);

        sharedpreferences = getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);

        SharedPreferences sharedPreferences = getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);

        buttonContinue.setOnClickListener(v -> {
            String code = editTextCountryCode.getText().toString().trim();
            String number = editTextPhone.getText().toString().trim();
            String userid = editTextuserid.getText().toString().trim();

            if (userid.isEmpty()) {
                editTextPhone.setError("UserId is required");
                return;
            }
            else if (number.isEmpty() || number.length() < 10) {
                editTextPhone.setError("Mobile Number is required");
                return;
            }


            String phoneNumber = code + number;

            Map<String, String> params = new HashMap<String, String>();
            params.put("uname", userid);
            params.put("MobileNo", number);


            JsonObjectRequest req = new JsonObjectRequest(Config.PROJECT_URL + "VerifyUserIdMobileNo", new JSONObject(params),
                    response -> {
                        try {
                            try {
                                if (response.getString("MSG").equalsIgnoreCase("Success")) {
                                    try {
                                        IntName = response.getString("IntName");
                                        MSG = response.getString("MSG");
                                        len = response.getString("MobileNo");
                                        fieldOffice = response.getString("fieldOffice");
                                        pwd = response.getString("pwd");
                                        status = response.getString("status");
                                        uname = response.getString("uname");

                                        SharedPreferences.Editor editor = sharedpreferences.edit();
                                        editor.putBoolean(LOGGEDIN_SHARED_PREF, true);
                                        editor.putString("uname", uname);
                                        editor.putString("pwd", pwd);

                                        editor.apply();
                                        Log.i("params", "getHeaders: " + response);

                                        Intent intent = new Intent(PhoneNumberActivity.this, VerifyPhoneActivity.class);
                                        intent.putExtra("username", userid);
                                        intent.putExtra("phoneNumber", number);
                                        intent.putExtra("code", code);
                                        intent.putExtra("pwd", pwd);
                                        startActivity(intent);


                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }


                                    VolleyLog.v("Response:%n %s", response.toString(4));
                                } else {
                                    Toast.makeText(PhoneNumberActivity.this, "Enter Valid number and Userid", Toast.LENGTH_LONG).show();
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                    }, error -> VolleyLog.e("Error: ", error.getMessage()));

            RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
            requestQueue.add(req);

        });
    }

    @Override
    protected void onStart() {
        super.onStart();
    }
}