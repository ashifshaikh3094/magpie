package in.aceventura.HansaMagpie;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

public class SharedClass {
    private static Context mContext;
    private static SharedClass mInstance;

    public SharedClass(Context mContext) {
        super();
        this.mContext = mContext;
    }

    private static final String SHARED_PREF_NAME = "mysharedpref12";
    //private static final String KEY_USER_NAME = "user_Id";
    public static final String KEY_REG_ID = "reg_id";
    private static final String KEY_USER_ID = "uname";
    private static final String KEY_PASSWORD = "pwd";
    private static final String KEY_IntName = "IntName";
    private static final String KEY_FieldOffice = "fieldoffice";
    public static final String KEY_ROLE_ID = "role_id";
    public static final String KEY_PARENT_ID = "parent_id";
    public static final String KEY_STUDENT_ID = "student_id";
    public static final String KEY_STUDENT_CLASS = "class_id";
    public static final String KEY_STUDENT_SECTION = "section_id";
    public static final String KEY_STUDENT_NAME = "first_name";
    public static final String KEY_ACADEMIC_YEAR = "academic_yr";
    public static final String KEY_COMMENT_ID = "comment_id";
    public static final String KEY_HOMEWORK_ID = "homework_id";
    public static final String KEY_PARENT_COMMENT = "parent_comment";
    public static final String KEY_PHONE_NO = "phone_no";
    public static final String T_USER_ID = "user_id";
    public static final String T_NAME = "name";
    public static final String T_REG_ID = "reg_id";
    public static final String T_ROLE_ID = "role_id";
    public static final String T_ACADEMIC_YEAR = "academic_yr";
    public static final String password = "password";
    public static final String loginDate = "login_date";
    public static final String jobName = "job_name";


    // todo profile details -05-05-2021
    public static final String AC_No = "ac_no";
    public static final String Address = "address";
    public static final String Bank = "bank";
    public static final String BankAcName = "bankAcName";
    public static final String Branch = "branch";
    public static final String Contact_Number = "contactnumber";
    public static final String IfscCode = "ifscCode";
    public static final String IntName = "namevalue";


    public static synchronized SharedClass getInstance(Context context) {
        if (mInstance == null) {
            mInstance = new SharedClass(context);
        }
        return mInstance;
    }

    //================================================================================
    public SharedClass saveSharedPrefrences(String key, String value) {
        Log.i("key " + key, " value " + value);
        SharedPreferences preferences = mContext.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        preferences.edit().putString(key, value).commit();
        return null;
    }

    public boolean isLoggedIn() {
        SharedPreferences sharedPreferences = mContext.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        if (sharedPreferences.getString(KEY_USER_ID, null) != null) {
            return true;
        }
        return false;
    }

    public String loadSharedPreference_UserId() {
        SharedPreferences pref = mContext.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        String stateid = pref.getString("uname", "");
        return stateid;
    }

    String loadSharedPreference_IntName() {
        SharedPreferences pref = mContext.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        String stateid = pref.getString("IntName", "");
        return stateid;
    }

    public String getPassword() {
        SharedPreferences pref = mContext.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        String stateid = pref.getString("password", "");
        return stateid;
    }

    boolean userLogin(String user_id, String password, String name, String fieldOffice) {

        SharedPreferences sharedPreferences = mContext.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();

        editor.putString(KEY_USER_ID, user_id);
        editor.putString(KEY_PASSWORD, password);
        editor.putString(KEY_IntName, name);
        editor.putString(KEY_FieldOffice, fieldOffice);
        editor.apply();

        return true;
    }

    boolean setuserProfile(String ac_no, String address,
                           String bank, String bankAcName, String branch,
                           String contact_number, String ifscCode, String intName) {
        Log.e("ProfileValue", "ValuesInsideShF" + ac_no);

        SharedPreferences sharedPreferences = mContext.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();


        editor.putString(AC_No, ac_no);
        editor.putString(Address, address);
        editor.putString(Bank, bank);
        editor.putString(BankAcName, bankAcName);
        editor.putString(Branch, branch);
        editor.putString(Contact_Number, contact_number);
        editor.putString(IfscCode, ifscCode);
        editor.putString(IntName, intName);

        editor.apply();
        Log.e("ProfileValue", "ValuesInsideShFSuccess" + ac_no);

        return true;
    }


    static boolean loginDate(String logInDate) {

        SharedPreferences sharedPreferences = mContext.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(loginDate, logInDate);
        editor.apply();
        return true;
    }


    public String getLoginDate() {
        SharedPreferences pref = mContext.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        return pref.getString("login_date", "");
    }

    public String getAC_No() {
        SharedPreferences pref = mContext.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        return pref.getString("ac_no", "");
    }

    public String getAddress() {
        SharedPreferences pref = mContext.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        return pref.getString("address", "");
    }

    public String getContact_Number() {
        SharedPreferences pref = mContext.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        return pref.getString("contactnumber", "");
    }

    public String getBankAcName() {
        SharedPreferences pref = mContext.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        return pref.getString("bankAcName", "");
    }

    public String getBank() {
        SharedPreferences pref = mContext.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        return pref.getString("bank", "");
    }

    public String getBranch() {
        SharedPreferences pref = mContext.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        return pref.getString("branch", "");
    }

    public String getIfscCode() {
        SharedPreferences pref = mContext.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        return pref.getString("ifscCode", "");
    }

    public String getIntName() {
        SharedPreferences pref = mContext.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        return pref.getString("namevalue", "");
    }

    public static boolean jobName(String job_name) {

        SharedPreferences sharedPreferences = mContext.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(jobName, job_name);
        editor.apply();
        return true;
    }


    public String getjobName() {
        SharedPreferences pref = mContext.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        return pref.getString("job_name", "");
    }

    public String getFieldOffice() {
        SharedPreferences pref = mContext.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        return pref.getString("fieldoffice", "");
    }

    public boolean logout() {
        SharedPreferences sharedPreferences = mContext.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.clear();
        editor.apply();
        return true;
    }

}
